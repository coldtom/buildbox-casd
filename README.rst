What is ``buildbox-casd``?
==========================

``buildbox-casd`` is a local cache and proxy for the
``ContentAddressableStorage`` services described by Bazel's
`Remote Execution API`_.

.. _Remote Execution API: https://docs.google.com/document/d/1AaGk7fOPByEvpAbqeXIyE8HX_A3_axxNnvroblTZ_6s

It also supports the `LocalCAS Protocol`_.

.. _LocalCAS Protocol: https://gitlab.com/BuildGrid/buildbox/buildbox-common/blob/master/protos/build/buildgrid/local_cas.proto


Usage
=====

``buildbox-casd`` can be configured in two modes:

* As a regular CAS server that stores blobs on disk, or

* as a CAS proxy. In this mode ``casd`` is pointed to a remote CAS server and it will forward all requests to it while caching data locally.

   buildbox-casd [OPTIONS] LOCAL_CACHE

This starts a daemon storing local cached data at the ``LOCAL_CACHE`` path.

The key parameters are:

* ``--instance=NAME``

  Name given to the new LocalCAS instance (if not set, an empty string).

* ``--verbose``

  Output additional information to stdout that may be useful when debugging.

* ``--cas-remote=URL``

  URL of the remote CAS server to which requests should be forwarded to
  (proxy mode).

  If none is given, ``casd`` behaves like a regular CAS server.

* ``--bind=URL``

  Endpoint where ``casd`` listens for incoming requests.

  By default a Unix domain socket in ``LOCAL_CACHE/casd.sock``.

::

    usage: ./buildbox-casd [OPTIONS] LOCAL_CACHE
        --instance=NAME             LocalCAS instance name (default "")
        --cas-remote=URL            URL for CAS service
        --cas-instance=NAME         Name of the CAS instance
        --cas-server-cert=PATH      Public server certificate for TLS (PEM-encoded)
        --cas-client-key=PATH       Private client key for TLS (PEM-encoded)
        --cas-client-cert=PATH      Public client certificate for TLS (PEM-encoded)
        --cas-retry-limit=INT       Number of times to retry on grpc errors
        --cas-retry-delay=SECONDS   How long to wait between grpc retries
        --bind                      Bind to "address:port" or UNIX socket in
                                    "unix:path".
                                    (Default: unix:LOCAL_CACHE/casd.sock)
        --quota-high=SIZE           Maximum local cache size (e.g., 50G or 2T)
        --quota-low=SIZE            Local cache size to retain on LRU expiry
                                    (Default: Half of high quota)
        --reserved=SIZE             Reserved disk space (headroom left when `quota_high`
                                    gets automatically reduced)
                                    (Default: 2G)
        --protect-session-blobs     Do not expire blobs created or used
                                    in the current session
        --log-level=LEVEL           "trace", "debug", "info", "warning", "error" (Default: "error").
        --verbose                   Set log level to DEBUG



**Note**: URLs can be of the form ``http(s)://address:port``, ``unix:path`` (with a relative or absolute path), or ``unix://path`` (with an absolute path).
