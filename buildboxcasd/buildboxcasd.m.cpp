/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_daemon.h>

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_logging.h>

#include <cstring>
#include <iostream>
#include <signal.h>
#include <string>
#include <unistd.h>

using namespace buildboxcommon;
using namespace buildboxcasd;

const std::string default_log_level_name =
    buildboxcommon::logging::logLevelToString.at(Daemon::defaultLogLevel());

const std::string default_reserved_space = "2G";

static Daemon s_daemon;

std::string logLevelList()
{
    std::stringstream res;

    const auto level_map = buildboxcommon::logging::logLevelToString;
    for (auto it = level_map.cbegin(); it != level_map.cend(); it++) {
        res << "\"" << it->second << "\"";
        if (std::next(it) != level_map.cend()) {
            res << ", ";
        }
    }

    return res.str();
}

static void usage(const std::string &name)
{
    std::clog << "usage: " << name << " [OPTIONS] LOCAL_CACHE\n";
    std::clog << "    --instance=NAME             LocalCAS instance name "
                 "(default \"\")\n";
    ConnectionOptions::printArgHelp(32, "CAS", "cas-");
    std::clog << "    --bind                      Bind to \"address:port\" or "
                 "UNIX socket in \n"
              << "                                \"unix:path\".\n"
              << "                                (Default: "
                 "unix:LOCAL_CACHE/casd.sock)\n";
    std::clog << "    --quota-high=SIZE           Maximum local cache size "
                 "(e.g., 50G or 2T)\n";
    std::clog << "    --quota-low=SIZE            "
                 "Local cache size to retain on LRU expiry\n"
              << "                                "
                 "(Default: Half of high quota)\n";
    std::clog
        << "    --reserved=SIZE             Reserved disk space "
           "(headroom left when `quota_high`\n"
        << "                                gets automatically reduced)\n"
        << "                                (Default: "
        << default_reserved_space << ")\n";
    std::cerr << "    --protect-session-blobs     Do not expire blobs created "
                 "or used\n"
              << "                                in the current session\n";
    std::clog << "    --log-level=LEVEL           " << logLevelList()
              << " (Default: \"" << default_log_level_name << "\").\n";
    std::clog << "    --verbose                   Set log level to DEBUG"
              << std::endl;
}

void invalidArgumentError(const std::string &program_name,
                          const std::string &message,
                          const std::string &argument = "")
{
    std::cerr << message;

    if (!argument.empty()) {
        std::cerr << " \"" << argument << "\"";
    }

    std::cerr << std::endl;
    usage(program_name);
    exit(1);
}

static int64_t parseSize(const std::string &value)
{
    const char *s = value.c_str();
    char *endptr;
    int64_t size = strtoll(s, &endptr, 10);

    if (endptr == s || size < 0) {
        // Invalid number
        return 0;
    }
    else if (strlen(endptr) == 0) {
        // Valid number without suffix
        return size;
    }
    else if (strlen(endptr) == 1) {
        // Valid number with suffix
        switch (*endptr) {
            case 'K':
                return size * 1000ll;
            case 'M':
                return size * 1000000ll;
            case 'G':
                return size * 1000000000ll;
            case 'T':
                return size * 1000000000000ll;
            default:
                // Invalid suffix
                return 0;
        }
    }
    else {
        // Valid number with invalid suffix
        return 0;
    }
}

static void signal_handler(int) { s_daemon.stop(); }

int main(int argc, char *argv[])
{
    // Handle SIGINT and SIGTERM for graceful shutdown
    struct sigaction act;
    memset(&act, 0, sizeof(act));
    act.sa_handler = signal_handler;
    sigaction(SIGINT, &act, nullptr);
    sigaction(SIGTERM, &act, nullptr);

    const std::string program_name(argv[0]);

    for (int arg = 1; arg < argc; arg++) {
        if (s_daemon.d_cas_server.parseArg(argv[arg], "cas-")) {
            // Argument was handled by server_opts.
            continue;
        }

        const std::string argument(argv[arg]);

        const bool is_long_option = (argument.substr(0, 2) == "--");
        // Long optional parameters:
        if (is_long_option) {
            const auto assign_separator =
                std::find(argument.cbegin(), argument.cend(), '=');
            const bool is_assignment_option =
                assign_separator != argument.cend();

            const std::string argument_name(argument.cbegin() + 2,
                                            assign_separator);

            // key=value:
            if (is_assignment_option) {
                const std::string value(assign_separator + 1, argument.cend());

                if (argument_name == "instance") {
                    s_daemon.d_instance_name = value;
                }
                else if (argument_name == "bind") {
                    s_daemon.d_bind_address = value;
                }
                else if (argument_name == "quota-high") {
                    s_daemon.d_quota_high = parseSize(value);
                    if (s_daemon.d_quota_high == 0) {
                        std::cerr << "Invalid size " << value << " for option "
                                  << argv[0] << "\n";
                        usage(program_name);
                        return 1;
                    }
                }
                else if (argument_name == "quota-low") {
                    s_daemon.d_quota_low = parseSize(value);
                    if (s_daemon.d_quota_low == 0) {
                        std::cerr << "Invalid size " << value << " for option "
                                  << argv[0] << "\n";
                        usage(program_name);
                        return 1;
                    }
                }
                else if (argument_name == "reserved") {
                    s_daemon.d_reserved_space = parseSize(value);
                    if (s_daemon.d_reserved_space == 0) {
                        std::cerr << "Invalid size " << value << " for option "
                                  << argv[0] << "\n";
                        usage(program_name);
                        return 1;
                    }
                }
                else if (argument_name == "log-level") {
                    std::string level_name(value);
                    std::transform(value.cbegin(), value.cend(),
                                   level_name.begin(), ::tolower);

                    try {
                        s_daemon.d_log_level =
                            buildboxcommon::logging::stringToLogLevel.at(
                                level_name);
                    }
                    catch (const std::out_of_range &) {
                        invalidArgumentError(program_name, "Invalid log level",
                                             value);
                        return 1;
                    }
                }
                else {
                    invalidArgumentError(program_name, "Invalid option",
                                         argument_name);
                }
            }
            // flags:
            else {
                if (argument_name == "help") {
                    usage(program_name);
                    return 0;
                }
                else if (argument_name == "verbose") {
                    s_daemon.d_log_level = buildboxcommon::LogLevel::DEBUG;
                }
                else if (argument_name == "protect-session-blobs") {
                    s_daemon.d_protect_session_blobs = true;
                }
                else {
                    invalidArgumentError(program_name, "Invalid flag",
                                         argument_name);
                }
            }
        }
        // Mandatory CAS path:
        else if (s_daemon.d_local_cache_path.empty()) {
            s_daemon.d_local_cache_path = argument;
        }
        else {
            invalidArgumentError(program_name, "Unexpected argument",
                                 argument);
        }
    }

    if (s_daemon.d_local_cache_path.empty()) {
        invalidArgumentError(program_name, "Local cache path is missing");
    }

    if (s_daemon.d_quota_high == 0 && s_daemon.d_quota_low > 0) {
        std::cerr << "--quota-low is supported only in combination with "
                     "--quota-high\n";
        usage(program_name);
        return 1;
    }

    if (s_daemon.d_quota_high > 0 && s_daemon.d_quota_low == 0) {
        // Default the low quota to half of the high quota
        s_daemon.d_quota_low = s_daemon.d_quota_high / 2;
    }

    if (s_daemon.d_reserved_space == 0) {
        s_daemon.d_reserved_space = parseSize(default_reserved_space);
    }

    s_daemon.runDaemon();
    return 0;
}
