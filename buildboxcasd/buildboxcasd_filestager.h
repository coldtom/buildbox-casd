/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_FILESTAGER_H
#define INCLUDED_BUILDBOXCASD_FILESTAGER_H

#include <buildboxcasd_localcas.h>

namespace buildboxcasd {

class FileStager {
    /* Allows staging the contents of directories stored in a LocalCAS
     * instance.
     */
  public:
    inline explicit FileStager(buildboxcasd::LocalCas *cas_storage)
        : d_cas_storage(cas_storage)
    {
    }

    /* Stage the contents under directory specified by `root_digest`.
     *
     * The optional `path` argument allows to specify where the directory
     * should be staged, but it must point to a non-existent or empty
     * directory that is in the same filesystem than the LocalCAS containing
     * the data.
     *
     * If the directory given is not empty, is in a different filesystem, or
     * if encounters a non-supported symlink, it throws
     * `std::invalid_argument`.
     *
     * Any filesystem errors encountered during the staging
     * throw `std::system_error`, aborting the staging and restoring back
     * the staging directory to its previous state (empty or non-existent).
     */
    virtual void stage(const Digest &root_digest, const std::string &path) = 0;

    virtual void unstage(const std::string &path,
                         bool delete_root = false) = 0;

  protected:
    buildboxcasd::LocalCas *d_cas_storage;
};
} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_FILESTAGER_H
