/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localcas.h>

#include <algorithm>
#include <dirent.h>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <pthread.h>
#include <sstream>
#include <stdexcept>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_temporaryfile.h>

#if __APPLE__
#define st_mtim st_mtimespec
#endif

using namespace buildboxcasd;

const std::string::size_type FsLocalCas::hashLength =
    buildboxcommon::CASHash::hash("").hash().size();

FsLocalCas::FsLocalCas(const std::string &root_path)
    : d_storage_root(root_path + "/cas"),
      d_objects_directory(d_storage_root + "/objects"),
      d_temp_directory(d_storage_root + "/tmp")
{
    BUILDBOX_LOG_INFO("Creating LocalCAS in " << d_storage_root);

    try {
        BUILDBOX_LOG_INFO("Creating root directory: " << root_path);
        buildboxcommon::FileUtils::create_directory(root_path.c_str());

        BUILDBOX_LOG_INFO("Creating cas directory: " << d_storage_root)
        buildboxcommon::FileUtils::create_directory(d_storage_root.c_str());

        BUILDBOX_LOG_INFO(
            "Creating objects directory: " << d_objects_directory);
        buildboxcommon::FileUtils::create_directory(
            d_objects_directory.c_str());

        BUILDBOX_LOG_INFO("Creating temp directory: " << d_temp_directory)
        buildboxcommon::FileUtils::create_directory(d_temp_directory.c_str());
    }
    catch (std::system_error &e) {
        BUILDBOX_LOG_ERROR("Could not create CAS directory structure in "
                           << d_storage_root);
        throw e;
    }
}

bool FsLocalCas::hasBlob(const Digest &digest)
{
    const std::string path = filePath(digest);
    const int utime_result = utimes(path.c_str(), NULL);

    if (utime_result != 0) {
        if (errno == ENOENT) { // Top directory and/or file do not exist.
            return false;
        }
        throw std::runtime_error("Could not update timestamp for " + path +
                                 ": " + std::string(strerror(errno)));
    }

    return true;
}

int FsLocalCas::hardLink(const std::string &source_path,
                         const std::string &destination_path)
{
    // link(oldpath, newpath)
    const int link_result =
        link(source_path.c_str(), destination_path.c_str());

    if (link_result != 0 && errno != EEXIST) {
        const std::string error_message =
            "Could not hard link temporary file " + source_path + " to " +
            destination_path + ": " + strerror(errno);
        BUILDBOX_LOG_ERROR(error_message);
        throw std::runtime_error(error_message);
    }

    return link_result;
}

void FsLocalCas::deleteFile(const std::string &path)
{
    const int delete_temp_file_result = unlink(path.c_str());
    if (delete_temp_file_result != 0) {
        BUILDBOX_LOG_ERROR("Could not remove temporary file "
                           << path << ": " << strerror(errno));
    }
}

bool FsLocalCas::writeBlob(const Digest &digest, const std::string &data)
{
    validateBlobDigest(digest, data);

    if (hasBlob(digest)) { // No need to perform the write.
        return false;
    }

    // Creating a temp file and writing the blob data into it...
    buildboxcommon::TemporaryFile temp_file(createTemporaryFile());
    const std::string temp_filename(temp_file.name());

    std::ofstream file(temp_filename, std::fstream::binary);
    file << data;
    file.close();

    if (!file.good()) {
        const std::string error_message = "Failed writing to temporary file " +
                                          temp_filename + ": " +
                                          strerror(errno);
        BUILDBOX_LOG_ERROR(error_message);

        if (errno == ENOSPC) {
            throw OutOfSpaceException(error_message);
        }
        else {
            throw std::runtime_error(error_message);
        }
    }

    return moveBlob(digest, temp_filename);
}

bool FsLocalCas::moveBlob(const Digest &digest, const std::string &path)
{
    // Checking that path is a file inside this instance's temp directory:
    const bool path_in_temp_dir =
        (path.substr(0, d_temp_directory.length()) == d_temp_directory);
    if (!path_in_temp_dir) {
        throw std::invalid_argument("`path` is not in " + d_temp_directory);
    }

    validateFileDigest(digest, path);
    return moveTemporaryFileToCas(digest, path);
}

bool FsLocalCas::moveTemporaryFileToCas(const Digest &digest,
                                        const std::string &path)
{
    // Getting the path where the blob should be stored in CAS.
    // If the parent subdirectory does not exist, it will be (atomically)
    // created:
    const std::string path_in_cas = filePath(digest, true);

    // Now we can make a hard link from the CAS directory to the
    // temporary file before it gets deleted. This has the effect of writing
    // the file atomically into the local CAS.
    // In the worst case, someone could beat us to placing another copy of
    // the blob in the CAS. But, because the contents will be the same, that is
    // not a problem.
    const int link_result = hardLink(path, path_in_cas);

    deleteFile(path);

    // Returning whether we performed the write or didn't have to due to the
    // blob being present.
    return (link_result == 0);
}

std::string FsLocalCas::readBlob(const Digest &digest, size_t offset,
                                 size_t length)
{
    const size_t data_size = static_cast<size_t>(digest.size_bytes());

    size_t bytes_to_read;
    if (length == npos) {
        bytes_to_read = std::max(size_t(0), data_size - offset);
    }
    else {
        bytes_to_read = length;
    }

    if (offset > data_size || (offset + bytes_to_read > data_size)) {
        throw std::out_of_range("The given interval is out of bounds.");
    }

    std::ifstream file = openFile(digest);
    // The file exists and is now open. (Otherwise, openFile() threw.)
    // The range that we want to read is inside the data.

    // (Even when reading 0 bytes we want to make sure that the digest is
    // present.)
    if (bytes_to_read == 0) {
        return "";
    }

    file.seekg(static_cast<std::streamoff>(offset), std::ios_base::beg);

    std::unique_ptr<char[]> buffer(new char[bytes_to_read]);
    file.read(buffer.get(), static_cast<std::streamsize>(bytes_to_read));

    if (file.good()) {
        return std::string(buffer.get(), bytes_to_read);
    }

    const std::string error_message =
        "Error reading: " + filePath(digest) + ": " + strerror(errno);
    BUILDBOX_LOG_ERROR(error_message);
    throw std::runtime_error(error_message);
}

std::string FsLocalCas::readBlob(const Digest &digest)
{
    return readBlob(digest, 0, LocalCas::npos);
}

std::string FsLocalCas::path(const Digest &digest)
{
    if (!hasBlob(digest)) {
        throw BlobNotFoundException(toString(digest));
    }

    return filePath(digest, false);
}

buildboxcommon::TemporaryDirectory FsLocalCas::createTemporaryDirectory()
{
    return buildboxcommon::TemporaryDirectory(d_temp_directory.c_str(),
                                              "cas-tmpdir");
}

buildboxcommon::TemporaryFile FsLocalCas::createTemporaryFile()
{
    return buildboxcommon::TemporaryFile(d_temp_directory.c_str(),
                                         "cas-tmpfile", 0644);
}

std::string FsLocalCas::filePath(const Digest &digest,
                                 bool create_parent_directory) const
{
    const std::string hash = digest.hash();
    const std::string directory_name = hash.substr(0, k_HASH_PREFIX_LENGTH);
    const std::string file_name = hash.substr(k_HASH_PREFIX_LENGTH);

    const std::string path_in_cas = d_objects_directory + "/" + directory_name;

    if (create_parent_directory) {
        buildboxcommon::FileUtils::create_directory(path_in_cas.c_str());
    }

    return path_in_cas + "/" + file_name;
}

void FsLocalCas::validateDigests(const Digest &given_digest,
                                 const Digest &computed_digest)
{
    if (given_digest == computed_digest) {
        return;
    }

    std::stringstream error_message;
    error_message << "Digest " << given_digest
                  << " does not match data (expected " << computed_digest
                  << ")";

    throw std::invalid_argument(error_message.str());
}
void FsLocalCas::validateBlobDigest(const Digest &digest,
                                    const std::string &data) const
{
    const Digest computed_digest = buildboxcommon::CASHash::hash(data);
    validateDigests(digest, computed_digest);
}

void FsLocalCas::validateFileDigest(const Digest &digest,
                                    const std::string &path) const
{
    const Digest computed_digest = buildboxcommon::CASHash::hashFile(path);
    validateDigests(digest, computed_digest);
}

std::ifstream FsLocalCas::openFile(const Digest &digest)
{
    const std::string path = filePath(digest);

    std::ifstream file;
    file.open(path, std::fstream::binary);

    if (file.fail()) {
        const auto error_code = errno;
        if (error_code == ENOENT) {
            throw BlobNotFoundException(toString(digest));
        }

        const std::string error_message =
            "Could not open file " + path + ": " + std::strerror(error_code);
        BUILDBOX_LOG_ERROR(error_message);
        throw std::runtime_error(error_message);
    }

    // Update timestamp
    hasBlob(digest);

    return file;
}

bool FsLocalCas::deleteBlob(const Digest &digest)
{
    const std::string path = filePath(digest);

    if (unlink(path.c_str()) != 0) {
        if (errno == ENOENT) {
            return false;
        }
        else {
            const std::string error_message =
                "Could not delete file " + path + ": " + std::strerror(errno);
            BUILDBOX_LOG_ERROR(error_message);
            throw std::system_error(errno, std::system_category());
        }
    }

    return true;
}

static void logUnexpectedObject(const std::string &path)
{
    BUILDBOX_LOG_ERROR("Unexpected entry in objects directory: " + path);
}

bool FsLocalCas::digestFromFile(const std::string &dirname,
                                const std::string &filename, Digest *digest,
                                struct stat *st) const
{
    std::string objPath = d_objects_directory + "/" + dirname + "/" + filename;

    if (stat(objPath.c_str(), st) != 0) {
        throw std::system_error(errno, std::system_category());
    }

    if (!S_ISREG(st->st_mode)) {
        return false;
    }

    const std::string hash = dirname + filename;
    // Basic hash validation
    if (hash.size() != FsLocalCas::hashLength) {
        return false;
    }

    digest->set_hash(hash);
    digest->set_size_bytes(st->st_size);

    return true;
}

class DirectoryIterator {
  public:
    DirectoryIterator(const std::string &path)
        : d_dir_stream(opendir(path.c_str()), closedir)
    {
        if (d_dir_stream == nullptr) {
            throw std::system_error(errno, std::system_category());
        }
    }

    struct dirent *next()
    {
        while (true) {
            auto entry = readdir(d_dir_stream.get());
            if (entry != nullptr && (strcmp(entry->d_name, ".") == 0 ||
                                     strcmp(entry->d_name, "..") == 0)) {
                // Skip "." and ".."
                continue;
            }
            return entry;
        }
    }

  private:
    std::unique_ptr<DIR, int (*)(DIR *)> d_dir_stream;
};

void FsLocalCas::listBlobs(digest_time_callback_type callback)
{
    auto dirIt = DirectoryIterator(d_objects_directory);
    for (auto entry = dirIt.next(); entry != nullptr; entry = dirIt.next()) {
        std::string subdirPath =
            std::string(d_objects_directory) + "/" + entry->d_name;

        // The objects directory should only contain hash prefix
        // subdirectories.
        if (strlen(entry->d_name) != k_HASH_PREFIX_LENGTH) {
            logUnexpectedObject(subdirPath);
            continue;
        }

        auto subdirIt = DirectoryIterator(subdirPath);
        for (auto objEntry = subdirIt.next(); objEntry != nullptr;
             objEntry = subdirIt.next()) {
            Digest digest;
            struct stat st;

            if (!digestFromFile(entry->d_name, objEntry->d_name, &digest,
                                &st)) {
                logUnexpectedObject(subdirPath + "/" + objEntry->d_name);
                continue;
            }

            // Use microseconds instead of nanoseconds as system_clock is
            // limited to microsecond resolution at least on macOS and even
            // on Linux the precision of file timestamps is limited by the
            // kernel timer frequency.
            auto timestamp =
                std::chrono::system_clock::from_time_t(st.st_mtim.tv_sec) +
                std::chrono::microseconds{st.st_mtim.tv_nsec / 1000};

            callback(digest, timestamp);
        }
    }
}

int64_t FsLocalCas::getAvailableDiskSpace()
{
    struct statvfs buf;
    statvfs(d_storage_root.c_str(), &buf);
    return static_cast<int64_t>(buf.f_bavail) * buf.f_bsize;
}

int64_t LocalCas::getDiskUsage()
{
    int64_t disk_usage = 0;

    listBlobs([&disk_usage](const buildboxcommon::Digest &digest,
                            buildboxcasd::blob_time_type) {
        disk_usage += static_cast<unsigned long>(digest.size_bytes());
    });

    return disk_usage;
}

int64_t LocalCas::getDiskQuota()
{
    // The value of 0 means no quota.
    return 0;
}
