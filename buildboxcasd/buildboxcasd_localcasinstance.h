/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_LOCALCASINSTANCE_H
#define INCLUDED_BUILDBOXCASD_LOCALCASINSTANCE_H

#include <buildboxcasd_casinstance.h>
#include <buildboxcasd_filestager.h>

#include <memory>

using namespace build::bazel::remote::execution::v2;

using grpc::Status;

namespace buildboxcasd {

class LocalCasInstance
    /* This class implements the logic for methods that service a CAS server,
     * as defined by the Remote Execution API.
     */
    : public CasInstance {
  public:
    explicit LocalCasInstance(std::shared_ptr<LocalCas> storage,
                              const std::string &instance_name);

    grpc::Status FindMissingBlobs(const FindMissingBlobsRequest &request,
                                  FindMissingBlobsResponse *response) override;

    grpc::Status BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                  BatchUpdateBlobsResponse *response) override;

    grpc::Status BatchReadBlobs(const BatchReadBlobsRequest &request,
                                BatchReadBlobsResponse *response) override;

    grpc::Status Write(WriteRequest *request_message,
                       ServerReader<WriteRequest> &request,
                       WriteResponse *response) override;

    // LocalCAS methods
    grpc::Status FetchTree(const FetchTreeRequest &request,
                           FetchTreeResponse *response) override;

    grpc::Status
    StageTree(const StageTreeRequest &stage_request,
              ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream)
        override;

    grpc::Status CaptureTree(const CaptureTreeRequest &request,
                             CaptureTreeResponse *response) override;

    grpc::Status CaptureFiles(const CaptureFilesRequest &request,
                              CaptureFilesResponse *response) override;

  protected:
    /* Writes to the local storage, returning the status code that should be
     * attached to the response to the client.
     */
    google::rpc::Status writeToLocalStorage(const Digest &digest,
                                            const std::string data);

    /* Moves a file to the local storage, returns a status code to return as a
     * response.
     */
    grpc::Status moveToLocalStorage(const Digest &digest,
                                    const std::string &path);

    /* Attempts to read from the local storage into `data` returning a gRPC
     * status code that can be embedded into a reply.
     *
     * If the status returned is not successful, `data` remains unmodified.
     */
    google::rpc::Status readFromLocalStorage(const Digest &digest,
                                             std::string *data) const;

    google::rpc::Status readFromLocalStorage(const Digest &digest,
                                             std::string *data, size_t offset,
                                             size_t limit) const;

    /*
     * Store digests locally, and upload to remote CAS.
     * Returns Tree digest for constructing the response.
     */
    virtual const Digest
    UploadAndStore(buildboxcommon::digest_string_map *const digest_map,
                   const Tree *path_tree, bool bypass_local_cache);

    // Walks a tree and makes sure that all its blobs are stored locally.
    // If a CAS client is available, it will try and fetch missing blobs from a
    // remote.
    // It returns an `OK` status if all the blobs contained in the tree are
    // available locally, `FAILED_PRECONDITION` otherwise.
    virtual Status prepareTreeForStaging(const Digest &root_digest) const;

    std::vector<Digest>
    digestsMissingFromDirectory(const Directory &directory,
                                bool file_blobs = true) const;

    std::shared_ptr<LocalCas> d_storage;

  private:
    bool hasBlob(const Digest &digest) override;

    std::string createTemporaryDirectory() const;

    // Returns whether all the contents of given tree are stored locally.
    bool treeIsAvailableLocally(const Digest &root_digest,
                                bool file_blobs = true) const;

    Status
    stage(const Digest &root_digest, const std::string &stage_path,
          bool delete_directory_on_error,
          ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream);

    /*
     * Bytestream API
     *
     * We will service resource names that look like:
     *    "{instance_name}/uploads/{uuid}/blobs/{hash}/{size}", or
     *    "{instance_name}/blobs/{hash}/{size}"
     *  with anything after "size" being ignored.
     */
    google::rpc::Status readBlob(const Digest &digest, std::string *data,
                                 size_t read_offset,
                                 size_t read_limit) override;

    google::rpc::Status writeBlob(const Digest &digest,
                                  const std::string &data) override;

    std::unique_ptr<FileStager> d_file_stager;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_LOCALCASINSTANCE_H
