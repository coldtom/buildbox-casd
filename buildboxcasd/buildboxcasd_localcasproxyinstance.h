/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_LOCALCASPROXYINSTANCE_H
#define INCLUDED_BUILDBOXCASD_LOCALCASPROXYINSTANCE_H

#include <buildboxcasd_localcasinstance.h>

using namespace build::bazel::remote::execution::v2;

using grpc::Status;

namespace buildboxcasd {

class LocalCasProxyInstance final : public LocalCasInstance {
    /* This class implements the logic for methods that service a CAS server
     * that also acts as a proxy pointing to a remote one.
     *
     * Inheriting from `LocalContentAddresableStorageInstance` avoids having
     * to reimplement behavior that is shared with that type of server.
     */
  public:
    explicit LocalCasProxyInstance(
        std::shared_ptr<LocalCas> storage,
        std::shared_ptr<buildboxcommon::Client> cas_client,
        const std::string &instance_name);

    grpc::Status FindMissingBlobs(const FindMissingBlobsRequest &request,
                                  FindMissingBlobsResponse *response) override;

    grpc::Status BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                  BatchUpdateBlobsResponse *response) override;

    grpc::Status BatchReadBlobs(const BatchReadBlobsRequest &request,
                                BatchReadBlobsResponse *response) override;

    grpc::Status Write(WriteRequest *request_message,
                       ServerReader<WriteRequest> &request,
                       WriteResponse *response) override;

    // LocalCAS methods
    grpc::Status
    FetchMissingBlobs(const FetchMissingBlobsRequest &request,
                      FetchMissingBlobsResponse *response) override;

    grpc::Status
    UploadMissingBlobs(const UploadMissingBlobsRequest &request,
                       UploadMissingBlobsResponse *response) override;

    grpc::Status FetchTree(const FetchTreeRequest &request,
                           FetchTreeResponse *response) override;

  protected:
    /*
     * Store digests locally, and upload to remote CAS.
     * Returns Tree digest for constructing the response.
     */
    const Digest
    UploadAndStore(buildboxcommon::digest_string_map *const digest_map,
                   const Tree *path_tree, bool bypass_local_cache) override;

    // Walks a tree and makes sure that all its blobs are stored locally.
    // If a CAS client is available, it will try and fetch missing blobs from a
    // remote.
    // It returns an `OK` status if all the blobs contained in the tree are
    // available locally, `FAILED_PRECONDITION` otherwise.
    Status prepareTreeForStaging(const Digest &root_digest) const override;

  private:
    std::shared_ptr<buildboxcommon::Client> d_cas_client;

    std::set<std::string>
    batchUpdateRemoteCas(const BatchUpdateBlobsRequest &request);

    // Fetches from a remote CAS the blobs in the given tree that are not
    // present locally.
    void fetchTreeMissingBlobs(const Digest &root_digest,
                               bool file_blobs = true) const;

    Directory fetchDirectory(const Digest &root_digest) const;

    /* Returns whether the given digest is available to be read from the
     * LocalCAS.
     *
     * If the digest is not stored locally, it will attempt to download it
     * from the remote CAS. If that fails, returns `false`.
     */
    bool hasBlob(const Digest &digest) override;

    google::rpc::Status readBlob(const Digest &digest, std::string *data,
                                 size_t read_offset,
                                 size_t read_limit) override;

    google::rpc::Status writeBlob(const Digest &digest,
                                  const std::string &data) override;

    grpc::Status uploadBlob(const Digest &digest,
                            const std::string &path) const;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_LOCALCASPROXYINSTANCE_H
