/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localcasproxyinstance.h>
#include <buildboxcommon_client.h>
#include <buildboxcommon_logging.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace buildboxcasd;
using namespace buildboxcommon;

LocalCasProxyInstance::LocalCasProxyInstance(
    std::shared_ptr<LocalCas> storage,
    std::shared_ptr<buildboxcommon::Client> cas_client,
    const std::string &instance_name)
    : LocalCasInstance(storage, instance_name), d_cas_client(cas_client)
{
}

grpc::Status
LocalCasProxyInstance::FindMissingBlobs(const FindMissingBlobsRequest &request,
                                        FindMissingBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasProxyInstance::FindMissingBlobs request for instance name \""
        << request.instance_name() << "\" for "
        << request.blob_digests().size() << " digest(s)");

    const std::vector<Digest> requested_digests(
        request.blob_digests().cbegin(), request.blob_digests().cend());

    const std::vector<Digest> digests_missing_in_remote =
        d_cas_client->findMissingBlobs(requested_digests);

    for (const Digest &digest : digests_missing_in_remote) {
        if (d_storage->hasBlob(digest)) {
            // If we have the blob stored locally, we implicitly update the
            // remote.
            try {
                d_cas_client->upload(d_storage->readBlob(digest), digest);
                // If the upload succeeded we know that the blob is now in
                // the remote. We can skip adding it to the returned blob
                // list.
                continue;
            }
            catch (const std::runtime_error &e) {
                BUILDBOX_LOG_ERROR("Error uploading " << toString(digest)
                                                      << ": " << e.what());
            }
        }

        Digest *entry = response->add_missing_blob_digests();
        entry->CopyFrom(digest);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalCasProxyInstance::BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                        BatchUpdateBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasProxyInstance::BatchUpdateBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.requests().size()
        << " blob(s)");

    for (const auto &blob : request.requests()) {
        writeToLocalStorage(blob.digest(), blob.data());
    }

    // We assume that the client called `FindMissingBlobs()` previous to
    // this call, so we will upload all the blobs in the request.
    const std::set<std::string> digests_not_uploaded =
        batchUpdateRemoteCas(request);

    for (const auto &blob : request.requests()) {
        google::rpc::Status status;
        if (digests_not_uploaded.count(blob.digest().hash())) {
            status.set_code(grpc::StatusCode::UNAVAILABLE);
            status.set_message("Could not update blob to remote CAS");
        }
        else {
            status.set_code(grpc::StatusCode::OK);
        }

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(blob.digest());
        entry->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalCasProxyInstance::BatchReadBlobs(const BatchReadBlobsRequest &request,
                                      BatchReadBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasProxyInstance::BatchReadBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.digests().size()
        << " digest(s)");

    // Checking the local storage first:
    std::vector<Digest> digests_missing_locally;
    for (const Digest &digest : request.digests()) {
        std::string data;
        const google::rpc::Status read_status =
            readFromLocalStorage(digest, &data);

        if (read_status.code() == grpc::StatusCode::OK) {
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->CopyFrom(read_status);
            entry->set_data(data);
        }
        else {
            digests_missing_locally.push_back(digest);
        }
    }

    if (digests_missing_locally.empty()) {
        BUILDBOX_LOG_INFO("retrieved all digest(s) for instance name \""
                          << request.instance_name()
                          << "\" from local storage");
        return grpc::Status::OK;
    }

    // Making a request for the blobs that we couldn't find locally:
    const buildboxcommon::Client::DownloadedData downloaded_data =
        d_cas_client->downloadBlobs(digests_missing_locally);

    size_t count = 0;
    for (const Digest &digest : digests_missing_locally) {
        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(digest);

        google::rpc::Status status;

        const auto digest_data_it = downloaded_data.find(digest.hash());
        if (digest_data_it != downloaded_data.cend()) {
            const std::string data = digest_data_it->second;
            status.set_code(grpc::StatusCode::OK);
            entry->set_data(data);

            writeToLocalStorage(digest, data);
            ++count;
        }
        else {
            status.set_code(grpc::StatusCode::NOT_FOUND);
            status.set_message("Blob could not be fetched from remote CAS");
        }

        entry->mutable_status()->CopyFrom(status);
    }

    BUILDBOX_LOG_INFO("downloaded "
                      << count << " out of " << digests_missing_locally.size()
                      << " digest(s) from the remote cas server");

    return grpc::Status::OK;
}

grpc::Status LocalCasProxyInstance::Write(WriteRequest *request_message,
                                          ServerReader<WriteRequest> &request,
                                          WriteResponse *response)
{
    response->set_committed_size(0);

    const auto buffer_file = d_storage->createTemporaryFile();

    Digest digest_to_write;
    const auto request_status = CasInstance::processWriteRequest(
        request_message, request, &digest_to_write, buffer_file.name());

    if (!request_status.ok()) {
        return request_status;
    }

    // Trying to upload the blob first:
    const auto upload_status = uploadBlob(digest_to_write, buffer_file.name());

    // Upload was successful. We can move the file directly to the CAS,
    // avoiding copies:
    const auto move_status =
        moveToLocalStorage(digest_to_write, buffer_file.name());

    if (move_status.ok()) {
        response->set_committed_size(digest_to_write.size_bytes());
    }

    return move_status;
}

bool LocalCasProxyInstance::hasBlob(const Digest &digest)
{
    try {
        if (!d_storage->hasBlob(digest)) {
            const std::string directory_entry =
                d_cas_client->fetchString(digest);
            d_storage->writeBlob(digest, directory_entry);
        }
        return true;
    }
    catch (const std::runtime_error &) {
        return false;
    }
}

google::rpc::Status LocalCasProxyInstance::readBlob(const Digest &digest,
                                                    std::string *data,
                                                    size_t read_offset,
                                                    size_t read_limit)
{
    const google::rpc::Status read_status =
        readFromLocalStorage(digest, data, read_offset, read_limit);
    if (read_status.code() == grpc::StatusCode::OK) {
        return read_status;
    }

    google::rpc::Status status;
    try {
        const std::string fetched_data = d_cas_client->fetchString(digest);

        if (read_limit == 0) {
            *data = fetched_data.substr(read_offset);
        }
        else {
            *data = fetched_data.substr(read_offset, read_limit);
        }

        status.set_code(grpc::StatusCode::OK);

        writeToLocalStorage(digest, fetched_data);
    }
    catch (const std::runtime_error &) {
        status.set_code(grpc::StatusCode::NOT_FOUND);
        status.set_message("Blob not found in local nor remote CAS");
    }

    return status;
}

google::rpc::Status LocalCasProxyInstance::writeBlob(const Digest &digest,
                                                     const std::string &data)
{
    BUILDBOX_LOG_INFO("LocalCasProxyInstance::writeBlob: writing digest of "
                      << digest.size_bytes() << " bytes");

    google::rpc::Status status;

    // Trying to upload the blob first:
    try {
        d_cas_client->upload(data, digest);
    }
    catch (const std::logic_error &) { // Digest does not match the data.
        status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
        return status;
    }
    catch (const std::runtime_error &) {
        status.set_code(grpc::StatusCode::UNAVAILABLE);
        return status;
    }

    // Upload was successful. We now write to the local storage
    // (if needed, LocalCas will skip the write if the file exists.):
    google::rpc::Status write_status = writeToLocalStorage(digest, data);
    status.set_code(write_status.code());

    return status;
}

grpc::Status LocalCasProxyInstance::uploadBlob(const Digest &digest,
                                               const std::string &path) const
{
    const int fd = open(path.c_str(), O_RDONLY);
    if (fd == -1) {
        std::ostringstream error_message;
        error_message << "Could not read intermediate file at \"" << path
                      << "\":" << strerror(errno);
        BUILDBOX_LOG_ERROR(error_message.str());
        return grpc::Status(grpc::StatusCode::INTERNAL, error_message.str());
    }

    grpc::Status upload_status;
    try {
        d_cas_client->upload(fd, digest);
        upload_status = grpc::Status::OK;
    }
    catch (const std::logic_error &e) { // Digest does not match the data.
        upload_status =
            grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, e.what());
    }
    catch (const std::runtime_error &e) {
        upload_status = grpc::Status(grpc::StatusCode::UNAVAILABLE, e.what());
    }

    close(fd);
    return upload_status;
}

std::set<std::string> LocalCasProxyInstance::batchUpdateRemoteCas(
    const BatchUpdateBlobsRequest &request)
{
    std::vector<buildboxcommon::Client::UploadRequest> upload_requests;
    upload_requests.reserve(static_cast<size_t>(request.requests_size()));

    for (const auto &blob : request.requests()) {
        upload_requests.emplace_back(blob.digest(), blob.data());
    }

    const std::vector<buildboxcommon::Client::UploadResult>
        blobs_not_uploaded = d_cas_client->uploadBlobs(upload_requests);

    std::set<std::string> res;
    for (const buildboxcommon::Client::UploadResult &upload_result :
         blobs_not_uploaded) {
        res.emplace(upload_result.digest.hash());
    }

    return res;
}

Status LocalCasProxyInstance::FetchMissingBlobs(
    const FetchMissingBlobsRequest &request,
    FetchMissingBlobsResponse *response)
{
    // Checking the local storage first:
    std::vector<Digest> digests_missing_locally;
    for (const Digest &digest : request.blob_digests()) {
        if (!d_storage->hasBlob(digest)) {
            digests_missing_locally.push_back(digest);
        }
    }

    if (digests_missing_locally.empty()) {
        return grpc::Status::OK;
    }

    // Making a request for the blobs that we couldn't find locally:
    const buildboxcommon::Client::DownloadedData downloaded_data =
        d_cas_client->downloadBlobs(digests_missing_locally);

    for (const Digest &digest : digests_missing_locally) {
        google::rpc::Status status;
        status.set_code(grpc::StatusCode::OK);

        const auto digest_data_it = downloaded_data.find(digest.hash());
        if (digest_data_it != downloaded_data.cend()) {
            const std::string data = digest_data_it->second;

            try {
                d_storage->writeBlob(digest, data);
            }
            catch (const std::runtime_error &e) {
                status.set_code(grpc::StatusCode::INTERNAL);
                status.set_message(
                    "Internal error while writing blob to local CAS: " +
                    std::string(e.what()));
            }
        }
        else {
            status.set_code(grpc::StatusCode::NOT_FOUND);
            status.set_message("Blob could not be fetched from remote CAS");
        }

        if (status.code() != grpc::StatusCode::OK) {
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->CopyFrom(status);
        }
    }

    return grpc::Status::OK;
}

Status LocalCasProxyInstance::UploadMissingBlobs(
    const UploadMissingBlobsRequest &request,
    UploadMissingBlobsResponse *response)
{
    // Construct std::vector from protobuf digest list
    auto pb_digests = request.blob_digests();
    std::vector<Digest> digests(pb_digests.begin(), pb_digests.end());

    const auto missing_digests = d_cas_client->findMissingBlobs(digests);

    std::vector<buildboxcommon::Client::UploadRequest> upload_requests;
    for (const Digest &digest : missing_digests) {
        google::rpc::Status status;
        status.set_code(grpc::StatusCode::OK);

        try {
            std::string path = d_storage->path(digest);
            upload_requests.emplace_back(
                buildboxcommon::Client::UploadRequest::from_path(digest,
                                                                 path));
        }
        catch (const BlobNotFoundException &) {
            status.set_code(grpc::StatusCode::NOT_FOUND);
            status.set_message("Blob not found in local cache");
        }
        catch (const std::runtime_error &e) {
            status.set_code(grpc::StatusCode::INTERNAL);
            status.set_message(e.what());
        }

        if (status.code() != grpc::StatusCode::OK) {
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->CopyFrom(status);
        }
    }

    const auto not_uploaded_blobs = d_cas_client->uploadBlobs(upload_requests);

    for (const buildboxcommon::Client::UploadResult &upload_result :
         not_uploaded_blobs) {
        google::rpc::Status status;
        status.set_code(upload_result.status.error_code());
        status.set_message(upload_result.status.error_message());

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(upload_result.digest);
        entry->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

Status
LocalCasProxyInstance::prepareTreeForStaging(const Digest &root_digest) const
{
    // Proxy mode. (We can try and fetch the blobs that might be missing.)
    try {
        fetchTreeMissingBlobs(root_digest);
        return grpc::Status::OK;
    }
    catch (const std::runtime_error &e) {
        const std::string error_message =
            "Error while fetching blobs that are missing locally for staging "
            "directory with root digest \"" +
            toString(root_digest) + "\": " + e.what();
        BUILDBOX_LOG_ERROR(error_message);
        return Status(grpc::StatusCode::FAILED_PRECONDITION, error_message);
    }
}

void LocalCasProxyInstance::fetchTreeMissingBlobs(const Digest &root_digest,
                                                  bool file_blobs) const
{
    const Directory directory = fetchDirectory(root_digest);
    // (This `fetchDirectory()` throws if it fails, in which case we'll abort.)

    const std::vector<Digest> missing_digests =
        digestsMissingFromDirectory(directory, file_blobs);
    const auto downloaded_blobs = d_cas_client->downloadBlobs(missing_digests);

    for (const Digest &digest : missing_digests) {
        if (downloaded_blobs.count(digest.hash()) == 0) {
            // If a single blob is missing we can't stage the directory,
            // aborting:
            throw std::runtime_error(
                "Could not fetch missing blob with digest \"" +
                toString(digest) + "\"");
        }
        this->d_storage->writeBlob(digest, downloaded_blobs.at(digest.hash()));
    }

    // Recursively fetching the missing blobs in each subdirectory:
    for (const DirectoryNode &dir : directory.directories()) {
        fetchTreeMissingBlobs(dir.digest(), file_blobs);
    }
}

Directory
LocalCasProxyInstance::fetchDirectory(const Digest &root_digest) const
{
    Directory directory;
    if (this->d_storage->hasBlob(root_digest) &&
        directory.ParseFromString(this->d_storage->readBlob(root_digest))) {
        return directory;
    }

    const std::string directory_blob = d_cas_client->fetchString(root_digest);
    if (!directory.ParseFromString(directory_blob)) {
        throw std::runtime_error(
            "Error parsing fetched Directory with digest " +
            toString(root_digest));
    }

    d_storage->writeBlob(root_digest, directory.SerializeAsString());
    return directory;
}

const Digest LocalCasProxyInstance::UploadAndStore(
    buildboxcommon::digest_string_map *const digest_map, const Tree *t,
    bool bypass_local_cache)
{
    std::vector<Client::UploadRequest> upload_vec;
    // vector size is digest_map + 1 for tree_digest
    upload_vec.reserve(digest_map->size() + 1);

    // If bypass_local_cache is not set, writes digest to localCas
    auto storeDigestsLocally = [&](auto digest, auto content) {
        if (!bypass_local_cache) {
            d_storage->writeBlob(digest, content);
        }
    };

    Directory d;
    for (const auto &it : *digest_map) {
        // Check if string is a directory or file.Directory d;
        if (d.ParseFromString(it.second)) {
            upload_vec.emplace_back(
                Client::UploadRequest(it.first, it.second));
            storeDigestsLocally(it.first, it.second);
        }
        else {
            std::string file_content =
                FileUtils::get_file_contents(it.second.c_str());
            upload_vec.emplace_back(
                Client::UploadRequest(it.first, file_content));
            storeDigestsLocally(it.first, file_content);
        }
    }

    Digest tree_digest = Digest();
    if (t != nullptr) {
        // Get and emplace back tree_digest.
        tree_digest = buildboxcommon::make_digest(*t);
        const std::string tree_message_string = t->SerializeAsString();
        upload_vec.emplace_back(
            Client::UploadRequest(tree_digest, tree_message_string));

        storeDigestsLocally(tree_digest, tree_message_string);
    }

    // upload to remote cas
    d_cas_client->uploadBlobs(upload_vec);

    return tree_digest;
}

Status LocalCasProxyInstance::FetchTree(const FetchTreeRequest &request,
                                        FetchTreeResponse *)
{
    auto root_digest = request.root_digest();

    try {
        fetchTreeMissingBlobs(root_digest, request.fetch_file_blobs());
        return grpc::Status::OK;
    }
    catch (const std::runtime_error &e) {
        const std::string error_message =
            "Error while fetching blobs for directory with root digest \"" +
            toString(root_digest) + "\": " + e.what();
        BUILDBOX_LOG_ERROR(error_message);
        return grpc::Status(grpc::StatusCode::NOT_FOUND, error_message);
    }
}
