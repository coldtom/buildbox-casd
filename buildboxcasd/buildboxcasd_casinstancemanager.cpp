/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_casinstancemanager.h>

namespace buildboxcasd {

CasInstanceManager::CasInstanceManager() {}

bool CasInstanceManager::addInstance(const std::string &instance_name,
                                     std::shared_ptr<CasInstance> instance)
{
    std::lock_guard<std::mutex> lock(d_instance_map_mutex);

    if (d_instance_map.find(instance_name) != d_instance_map.end()) {
        return false;
    }

    d_instance_map[instance_name] = instance;
    return true;
}

std::shared_ptr<CasInstance>
CasInstanceManager::getInstance(const std::string &instance_name)
{
    std::lock_guard<std::mutex> lock(d_instance_map_mutex);

    auto it = d_instance_map.find(instance_name);
    if (it == d_instance_map.end()) {
        return nullptr;
    }

    return it->second;
}

} // namespace buildboxcasd
