/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_casserver.h>
#include <buildboxcasd_localcasinstance.h>
#include <buildboxcasd_localcasproxyinstance.h>
#include <buildboxcommon_grpcretry.h>
#include <buildboxcommon_logging.h>

namespace buildboxcasd {

using namespace build::bazel::remote::execution::v2;
using namespace google::bytestream;
using namespace google::protobuf::util;
using namespace google::protobuf;
using namespace buildboxcommon;

using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::Status;

CasService::CasService(std::shared_ptr<LocalCas> cas_storage)
    : d_cas_storage(cas_storage),
      d_instance_manager(std::make_shared<CasInstanceManager>()),
      d_remote_execution_cas_servicer(
          std::make_shared<CasRemoteExecutionServicer>(d_instance_manager)),
      d_cas_bytestream_servicer(
          std::make_shared<CasBytestreamServicer>(d_instance_manager)),
      d_local_cas_servicer(std::make_shared<LocalCasServicer>(
          d_instance_manager, d_cas_storage)),
      d_capabilities_servicer(
          std::make_shared<CapabilitiesServicer>(d_instance_manager))
{
}

CasService::CasService(std::shared_ptr<LocalCas> cas_storage,
                       const std::string &instance_name)
    : CasService(cas_storage)
{
    auto instance =
        std::make_shared<LocalCasInstance>(d_cas_storage, instance_name);
    d_instance_manager->addInstance(instance_name, instance);
}

CasService::CasService(std::shared_ptr<LocalCas> cas_storage,
                       std::shared_ptr<buildboxcommon::Client> cas_client,
                       const std::string &instance_name)
    : CasService(cas_storage)
{
    auto instance = std::make_shared<LocalCasProxyInstance>(
        d_cas_storage, cas_client, instance_name);
    d_instance_manager->addInstance(instance_name, instance);
}

/*
 * Remote Execution API: Content Addressable Storage service
 *
 */
CasRemoteExecutionServicer::CasRemoteExecutionServicer(
    std::shared_ptr<CasInstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
}

#define CALL_INSTANCE(servicer_function)                                      \
    {                                                                         \
        auto _instance =                                                      \
            this->d_instance_manager->getInstance(request->instance_name());  \
        if (_instance != nullptr) {                                           \
            return _instance->servicer_function;                              \
        }                                                                     \
        else {                                                                \
            return INVALID_INSTANCE_NAME_ERROR(request)                       \
        }                                                                     \
    }

#define INVALID_INSTANCE_NAME_ERROR(request)                                  \
    grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,                          \
                 "Invalid instance name \"" + request->instance_name() +      \
                     "\"");

Status CasRemoteExecutionServicer::FindMissingBlobs(
    ServerContext *, const FindMissingBlobsRequest *request,
    FindMissingBlobsResponse *response)
{
    CALL_INSTANCE(FindMissingBlobs(*request, response));
}

Status CasRemoteExecutionServicer::BatchUpdateBlobs(
    ServerContext *, const BatchUpdateBlobsRequest *request,
    BatchUpdateBlobsResponse *response)
{
    CALL_INSTANCE(BatchUpdateBlobs(*request, response));
}

Status CasRemoteExecutionServicer::BatchReadBlobs(
    ServerContext *, const BatchReadBlobsRequest *request,
    BatchReadBlobsResponse *response)
{

    // Checking that the blobs returned in the response do not exceed the gRPC
    // message size limit. We use the upper bound set in the CAS client.
    const auto response_size_limit = static_cast<google::protobuf::int64>(
        buildboxcommon::Client::bytestreamChunkSizeBytes());

    google::protobuf::int64 bytes_requested = 0;
    for (const Digest &digest : request->digests()) {
        bytes_requested += digest.size_bytes();
    }

    if (bytes_requested > response_size_limit) {
        return grpc::Status(
            grpc::StatusCode::INVALID_ARGUMENT,
            "Sum of bytes requested exceeds the gRPC message size limit: " +
                std::to_string(bytes_requested) + " > " +
                std::to_string(response_size_limit) + " bytes");
    }

    CALL_INSTANCE(BatchReadBlobs(*request, response));
}

Status
CasRemoteExecutionServicer::GetTree(ServerContext *,
                                    const GetTreeRequest *request,
                                    ServerWriter<GetTreeResponse> *stream)
{
    CALL_INSTANCE(GetTree(*request, stream));
}

CasBytestreamServicer::CasBytestreamServicer(
    std::shared_ptr<CasInstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
}

Status CasBytestreamServicer::Read(ServerContext *, const ReadRequest *request,
                                   ServerWriter<ReadResponse> *writer)
{
    const std::string &resource_name = request->resource_name();
    std::string instance_name = readInstanceName(resource_name);

    auto instance = this->d_instance_manager->getInstance(instance_name);
    if (instance == nullptr) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "Invalid instance name.");
    }

    return instance->Read(*request, writer);
}

Status CasBytestreamServicer::Write(ServerContext *,
                                    ServerReader<WriteRequest> *request,
                                    WriteResponse *response)
{
    WriteRequest request_message;

    if (!request->Read(&request_message)) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "No request received.");
    }

    const std::string &resource_name = request_message.resource_name();
    std::string instance_name = writeInstanceName(resource_name);

    auto instance = this->d_instance_manager->getInstance(instance_name);
    if (instance == nullptr) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "Invalid instance name.");
    }

    return instance->Write(&request_message, *request, response);
}

std::string
CasBytestreamServicer::readInstanceName(const std::string &resource_name) const
{
    return instanceNameFromResourceName(resource_name, "blobs");
}

std::string CasBytestreamServicer::writeInstanceName(
    const std::string &resource_name) const
{
    return instanceNameFromResourceName(resource_name, "uploads");
}

std::string CasBytestreamServicer::instanceNameFromResourceName(
    const std::string &resource_name, const std::string &expected_root) const
{

    // "expected_root/..."
    if (resource_name.substr(0, expected_root.size()) == expected_root) {
        return ""; // No instance name specified.
    }

    // "{instance_name}/expected_root/..."
    const auto instance_name_end = resource_name.find("/" + expected_root);
    // (According to the RE specification: "[...] the `instance_name`
    // is an identifier, possibly containing multiple path segments [...]".)
    return resource_name.substr(0, instance_name_end);
}

CapabilitiesServicer::CapabilitiesServicer(
    std::shared_ptr<CasInstanceManager> instance_manager)
    : d_instance_manager(instance_manager)
{
    CacheCapabilities *const cache_capabilities =
        d_server_capabilities.mutable_cache_capabilities();

    cache_capabilities->add_digest_function(CASHash::digestFunction());

    cache_capabilities->set_symlink_absolute_path_strategy(
        SymlinkAbsolutePathStrategy_Value_ALLOWED);

    // Since we are running locally, we do not limit the size of
    // batches:
    cache_capabilities->set_max_batch_total_size_bytes(0);

    ActionCacheUpdateCapabilities *const action_cache_update_capabilities =
        cache_capabilities->mutable_action_cache_update_capabilities();
    action_cache_update_capabilities->set_update_enabled(false);
}

Status
CapabilitiesServicer::GetCapabilities(grpc::ServerContext *,
                                      const GetCapabilitiesRequest *request,
                                      ServerCapabilities *response)
{
    if (this->d_instance_manager->getInstance(request->instance_name()) !=
        nullptr) {
        response->CopyFrom(d_server_capabilities);
        return grpc::Status::OK;
    }

    return INVALID_INSTANCE_NAME_ERROR(request);
}

LocalCasServicer::LocalCasServicer(
    std::shared_ptr<CasInstanceManager> instance_manager,
    std::shared_ptr<LocalCas> storage)
    : d_instance_manager(instance_manager), d_storage(storage)
{
}

Status
LocalCasServicer::FetchMissingBlobs(ServerContext *,
                                    const FetchMissingBlobsRequest *request,
                                    FetchMissingBlobsResponse *response)
{
    CALL_INSTANCE(FetchMissingBlobs(*request, response));
}

Status
LocalCasServicer::UploadMissingBlobs(ServerContext *,
                                     const UploadMissingBlobsRequest *request,
                                     UploadMissingBlobsResponse *response)
{
    CALL_INSTANCE(UploadMissingBlobs(*request, response));
}

Status LocalCasServicer::FetchTree(ServerContext *,
                                   const FetchTreeRequest *request,
                                   FetchTreeResponse *response)
{
    CALL_INSTANCE(FetchTree(*request, response));
}

Status LocalCasServicer::UploadTree(ServerContext *,
                                    const UploadTreeRequest *request,
                                    UploadTreeResponse *response)
{
    CALL_INSTANCE(UploadTree(*request, response));
}

Status LocalCasServicer::StageTree(
    ServerContext *,
    ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream)
{
    StageTreeRequest stage_request;
    if (!stream->Read(&stage_request)) {
        return Status(grpc::StatusCode::INTERNAL, "Could not read request.");
    }

    StageTreeRequest *request = &stage_request;
    CALL_INSTANCE(StageTree(stage_request, stream));
}

Status LocalCasServicer::CaptureTree(ServerContext *,
                                     const CaptureTreeRequest *request,
                                     CaptureTreeResponse *response)
{
    CALL_INSTANCE(CaptureTree(*request, response));
}

Status LocalCasServicer::CaptureFiles(ServerContext *,
                                      const CaptureFilesRequest *request,
                                      CaptureFilesResponse *response)
{
    CALL_INSTANCE(CaptureFiles(*request, response));
}

Status LocalCasServicer::GetInstanceNameForRemote(
    ServerContext *, const GetInstanceNameForRemoteRequest *request,
    GetInstanceNameForRemoteResponse *response)
{
    /* Use hash of request message as instance name */
    const std::string request_str = request->SerializeAsString();
    const Digest request_digest = CASHash::hash(request_str);
    const std::string instance_name = request_digest.hash();

    auto instance = d_instance_manager->getInstance(instance_name);
    if (instance == nullptr) {
        /* First request for this remote, instantiate client. */

        auto connection_options = buildboxcommon::ConnectionOptions();
        connection_options.d_url = request->url().c_str();
        connection_options.d_instanceName = request->instance_name().c_str();
        connection_options.d_serverCert = request->server_cert().c_str();
        connection_options.d_clientKey = request->client_key().c_str();
        connection_options.d_clientCert = request->client_cert().c_str();

        auto client = std::make_shared<Client>();

        try {
            client->init(connection_options);
        }
        catch (const GrpcError &e) {
            /* Failed to initialize connection to server. */
            BUILDBOX_LOG_WARNING("Failed to connect to " << request->url()
                                                         << ": " << e.what());
            return e.status;
        }

        auto instance = std::make_shared<LocalCasProxyInstance>(
            d_storage, client, instance_name);
        d_instance_manager->addInstance(instance_name, instance);
    }

    response->set_instance_name(instance_name);

    return grpc::Status::OK;
}

Status LocalCasServicer::GetLocalDiskUsage(ServerContext *,
                                           const GetLocalDiskUsageRequest *,
                                           GetLocalDiskUsageResponse *response)
{
    response->set_size_bytes(d_storage->getDiskUsage());
    response->set_quota_bytes(d_storage->getDiskQuota());

    return grpc::Status::OK;
}

} // namespace buildboxcasd
