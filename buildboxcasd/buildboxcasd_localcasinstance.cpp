/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localcasinstance.h>

#include <buildboxcasd_hardlinkstager.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>

using namespace buildboxcasd;
using namespace buildboxcommon;

/*
 *   Helper method that stores err in status, and logs the message.
 */
void logAndStoreMessage(const std::string &err, google::rpc::Status &status,
                        grpc::StatusCode code)
{
    status.set_message(err);
    status.set_code(code);
    BUILDBOX_LOG_ERROR(err);
}

LocalCasInstance::LocalCasInstance(std::shared_ptr<LocalCas> storage,
                                   const std::string &instance_name)
    : CasInstance(instance_name), d_storage(storage),
      d_file_stager(std::make_unique<HardLinkStager>(storage.get()))
{
}

grpc::Status
LocalCasInstance::FindMissingBlobs(const FindMissingBlobsRequest &request,
                                   FindMissingBlobsResponse *response)
{
    // Returns a list of digests that are *not* in the CAS.
    BUILDBOX_LOG_INFO(
        "LocalCasInstance::FindMissingBlobs request for instance name \""
        << request.instance_name() << "\" for "
        << request.blob_digests().size() << " digest(s)");

    for (const Digest &digest : request.blob_digests()) {
        bool blob_in_cas = false;

        try {
            blob_in_cas = d_storage->hasBlob(digest);
        }
        catch (const std::runtime_error &) {
            BUILDBOX_LOG_ERROR("Could not determine if "
                               << digest.hash() << "is in local CAS.");
        }

        if (!blob_in_cas) {
            Digest *entry = response->add_missing_blob_digests();
            entry->CopyFrom(digest);
        }
    }
    return grpc::Status::OK;
}

grpc::Status
LocalCasInstance::BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                   BatchUpdateBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasInstance::BatchUpdateBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.requests().size()
        << " blob(s)");

    for (const auto &blob : request.requests()) {
        const google::rpc::Status status =
            writeToLocalStorage(blob.digest(), blob.data());

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(blob.digest());
        entry->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalCasInstance::BatchReadBlobs(const BatchReadBlobsRequest &request,
                                 BatchReadBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasInstance::BatchReadBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.digests().size()
        << " digest(s)");

    for (const Digest &digest : request.digests()) {
        std::string data;
        const google::rpc::Status status = readFromLocalStorage(digest, &data);

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(digest);
        entry->mutable_status()->CopyFrom(status);
        entry->set_data(data);
    }
    return grpc::Status::OK;
}

grpc::Status LocalCasInstance::Write(WriteRequest *request_message,
                                     ServerReader<WriteRequest> &request,
                                     WriteResponse *response)
{
    response->set_committed_size(0);

    const auto buffer_file = d_storage->createTemporaryFile();

    Digest digest_to_write;
    const auto request_status = CasInstance::processWriteRequest(
        request_message, request, &digest_to_write, buffer_file.name());

    if (!request_status.ok()) {
        return request_status;
    }

    // We can move the file directly to the CAS, avoiding copies:
    const auto move_status =
        moveToLocalStorage(digest_to_write, buffer_file.name());

    if (move_status.ok()) {
        response->set_committed_size(digest_to_write.size_bytes());
    }

    return move_status;
}

google::rpc::Status
LocalCasInstance::writeToLocalStorage(const Digest &digest,
                                      const std::string data)
{
    google::rpc::Status status;

    try {
        d_storage->writeBlob(digest, data);
        status.set_code(grpc::StatusCode::OK);
    }
    catch (const std::invalid_argument &) {
        status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
        status.set_message(
            "The size of the data does not match the size defined "
            "in the digest.");
    }
    catch (const std::runtime_error &e) {
        status.set_code(grpc::StatusCode::INTERNAL);
        status.set_message("Internal error while writing blob to local CAS: " +
                           std::string(e.what()));
    }

    return status;
}

grpc::Status LocalCasInstance::moveToLocalStorage(const Digest &digest,
                                                  const std::string &path)
{
    try {
        d_storage->moveBlob(digest, path);
        return grpc::Status::OK;
    }
    catch (const std::invalid_argument &e) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "Invalid argument: " + std::string(e.what()));
    }
    catch (const std::runtime_error &e) {
        return grpc::Status(grpc::StatusCode::INTERNAL,
                            "Internal error while moving blob to local CAS: " +
                                std::string(e.what()));
    }
}

google::rpc::Status
LocalCasInstance::readFromLocalStorage(const Digest &digest,
                                       std::string *data) const
{
    return readFromLocalStorage(digest, data, 0, 0);
}

google::rpc::Status
LocalCasInstance::readFromLocalStorage(const Digest &digest, std::string *data,
                                       size_t offset, size_t limit) const
{
    google::rpc::Status status;

    try {
        const size_t read_length = (limit > 0) ? limit : LocalCas::npos;
        *data = d_storage->readBlob(digest, offset, read_length);
        status.set_code(grpc::StatusCode::OK);
    }
    catch (const BlobNotFoundException &) {
        status.set_code(grpc::StatusCode::NOT_FOUND);
        status.set_message("Blob not found in the local CAS.");
    }
    catch (const std::out_of_range &) {
        status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
        status.set_message("Read interval is out of range.");
    }
    catch (const std::runtime_error &e) {
        status.set_code(grpc::StatusCode::INTERNAL);
        status.set_message(
            "Internal error while fetching blob in local CAS: " +
            std::string(e.what()));
    }

    return status;
}

bool LocalCasInstance::hasBlob(const Digest &digest)
{
    return d_storage->hasBlob(digest);
}

google::rpc::Status LocalCasInstance::readBlob(const Digest &digest,
                                               std::string *data,
                                               size_t read_offset,
                                               size_t read_limit)
{
    return readFromLocalStorage(digest, data, read_offset, read_limit);
}

google::rpc::Status LocalCasInstance::writeBlob(const Digest &digest,
                                                const std::string &data)
{
    BUILDBOX_LOG_INFO("LocalCasInstance::writeBlob: writing digest of "
                      << digest.size_bytes() << " bytes");
    return writeToLocalStorage(digest, data);
}

Status LocalCasInstance::StageTree(
    const StageTreeRequest &stage_request,
    ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream)
{
    // Before we can stage the tree, we need to make sure that all of its
    // contents are stored locally.
    const auto tree_availability_status =
        prepareTreeForStaging(stage_request.root_digest());
    if (!tree_availability_status.ok()) {
        return tree_availability_status;
    }
    // All the blobs required to stage are present in the local storage.

    /* There are 3 cases for `stage_request.path()`:
     *  a) Empty string (we need to create a temporary directory),
     *  b) A path to a directory that doesn't exist yet
     *  (and that will be created by the stager), or
     *  c) A path to an empty directory.
     *
     * In cases a) and b), cleanup involves deleting the directory. But for c)
     * we just want to empty it.
     */

    bool stage_request_path_exists;
    std::string stage_path;
    if (stage_request.path().empty()) {
        stage_request_path_exists = false;

        try {
            stage_path = createTemporaryDirectory();
        }
        catch (const std::system_error &e) {
            std::ostringstream error_message;
            error_message << "Could not create temporary directory to stage: "
                          << stage_request.root_digest() << ": " << e.what();

            BUILDBOX_LOG_ERROR(error_message.str());
            return Status(grpc::StatusCode::INTERNAL, error_message.str());
        }
    }
    else {
        stage_request_path_exists = buildboxcommon::FileUtils::is_directory(
            stage_request.path().c_str());
        stage_path = stage_request.path();
    }

    // Stage the files with the stage method which will also write back
    // onto the stream to the client to inform them that the staging is
    // done.
    Status stage_status = stage(stage_request.root_digest(), stage_path,
                                !stage_request_path_exists, stream);
    if (!stage_status.ok()) {
        return stage_status;
    }

    // The staging operation was successful.
    // Now we wait for a second request, which we expect to be empty:
    StageTreeRequest cleanup_request;
    const bool received_cleanup_request = stream->Read(&cleanup_request);

    if (received_cleanup_request &&
        (!cleanup_request.path().empty() ||
         !(cleanup_request.root_digest() == Digest()))) {
        std::ostringstream error_message;
        error_message << "Unexpected non-empty request after staging "
                      << stage_request.root_digest() << " in " << stage_path;
        stage_status =
            Status(grpc::StatusCode::INVALID_ARGUMENT, error_message.str());
    }

    try {
        d_file_stager->unstage(stage_path, !stage_request_path_exists);
    }
    catch (const std::system_error &e) {
        BUILDBOX_LOG_WARNING("Caught " << e.what() << " while unstaging ["
                                       << stage_path);
    }

    // Sending an empty reply that signals that we are done cleaning:
    if (received_cleanup_request) {
        stream->Write(StageTreeResponse());
    }
    return stage_status;
}

bool LocalCasInstance::treeIsAvailableLocally(const Digest &root_digest,
                                              bool file_blobs) const
{
    Directory directory;
    if (!d_storage->hasBlob(root_digest) ||
        !directory.ParseFromString(d_storage->readBlob(root_digest))) {
        return false;
    }

    if (file_blobs) {
        for (const FileNode &file : directory.files()) {
            if (!this->d_storage->hasBlob(file.digest())) {
                return false;
            }
        }
    }

    for (const DirectoryNode &dir : directory.directories()) {
        if (!treeIsAvailableLocally(dir.digest(), file_blobs)) {
            return false;
        }
    }

    return true;
}

Status LocalCasInstance::prepareTreeForStaging(const Digest &root_digest) const
{
    // Server mode. (All the blobs must be available locally.)
    if (treeIsAvailableLocally(root_digest)) {
        return grpc::Status::OK;
    }
    return Status(grpc::StatusCode::FAILED_PRECONDITION,
                  "Tree is not completely available from LocalCAS.");
}

std::vector<Digest>
LocalCasInstance::digestsMissingFromDirectory(const Directory &directory,
                                              bool file_blobs) const
{
    std::vector<Digest> missing_digests;
    missing_digests.reserve(static_cast<size_t>(directory.files_size()) +
                            static_cast<size_t>(directory.directories_size()));

    if (file_blobs) {
        for (const FileNode &file : directory.files()) {
            if (!this->d_storage->hasBlob(file.digest())) {
                missing_digests.push_back(file.digest());
            }
        }
    }
    for (const DirectoryNode &dir : directory.directories()) {
        if (!this->d_storage->hasBlob(dir.digest())) {
            missing_digests.push_back(dir.digest());
        }
    }
    missing_digests.shrink_to_fit();
    return missing_digests;
}

Status LocalCasInstance::stage(
    const Digest &root_digest, const std::string &stage_path,
    bool delete_directory_on_error,
    ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream)
{
    Status stage_status;
    try {
        d_file_stager->stage(root_digest, stage_path);

        StageTreeResponse response;
        response.set_path(stage_path);
        stream->Write(response);

        stage_status = grpc::Status::OK;
    }
    catch (const std::invalid_argument &e) {
        stage_status = Status(grpc::StatusCode::INVALID_ARGUMENT, e.what());
    }
    catch (const std::runtime_error &e) {
        stage_status = Status(grpc::StatusCode::INTERNAL, e.what());
    }

    if (!stage_status.ok() && delete_directory_on_error) {
        // `stage()` rolls back the status of `stage_path` when the
        // operation aborts, leaving it as it originally was.
        // However, if we created a temporary directory, we want to delete
        // it.
        try {
            buildboxcommon::FileUtils::delete_directory(stage_path.c_str());
        }
        catch (const std::system_error &e) {
            BUILDBOX_LOG_ERROR("Could not delete directory "
                               << stage_path << ":" << e.what());
        }
    }

    return stage_status;
}

std::string LocalCasInstance::createTemporaryDirectory() const
{
    buildboxcommon::TemporaryDirectory stage_directory =
        d_storage->createTemporaryDirectory();
    stage_directory.setAutoRemove(false);
    return std::string(stage_directory.name());
}

Status LocalCasInstance::CaptureTree(const CaptureTreeRequest &request,
                                     CaptureTreeResponse *response)
{
    for (const std::string &path : request.path()) {

        auto resp = response->add_responses();
        google::rpc::Status status;
        std::string error_message;

        if (path.empty() || !FileUtils::is_directory(path.c_str()) ||
            path[0] != '/') {
            error_message = "Path: " + path +
                            " is empty, not a directory, or not an absolute "
                            "path.";
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::NOT_FOUND);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }

        digest_string_map digest_map;
        Tree path_tree;
        NestedDirectory nest_dir;
        Digest tree_digest;

        try {
            nest_dir = make_nesteddirectory(path.c_str(), &digest_map);
            path_tree = nest_dir.to_tree();
        }
        // Catch system errors thrown in make_nested.
        catch (const std::system_error &e) {
            error_message =
                "System error in `make_nesteddirectory()` for path \"" + path +
                "\": " + e.what();
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::INTERNAL);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }

        // Upload to remote CAS and store locally if bypass_local_cache() is
        // not set
        nest_dir.to_digest(&digest_map);
        try {
            tree_digest = UploadAndStore(&digest_map, &path_tree,
                                         request.bypass_local_cache());
        }
        catch (const OutOfSpaceException &e) {
            error_message = "Out of space error in `writeBlob()` for path \"" +
                            path + "\": " + e.what();
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::RESOURCE_EXHAUSTED);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }
        catch (const std::runtime_error &e) {
            error_message = "Runtime error uploading and storing path \"" +
                            path + "\": " + e.what();
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::INTERNAL);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }
        catch (const std::invalid_argument &e) {
            error_message =
                "Invalid argument error uploading and storing path \"" + path +
                "\": " + e.what();
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::INTERNAL);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }

        resp->mutable_tree_digest()->CopyFrom(tree_digest);
        resp->set_path(path);

        status.set_code(grpc::StatusCode::OK);
        resp->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

Status LocalCasInstance::CaptureFiles(const CaptureFilesRequest &request,
                                      CaptureFilesResponse *response)
{
    for (const std::string &path : request.path()) {

        auto resp = response->add_responses();
        google::rpc::Status status;
        std::string error_message;

        if (path.empty() || FileUtils::is_directory(path.c_str())) {
            error_message = "Path: " + path + " is empty, or is a directory.";
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::NOT_FOUND);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }
        try {
            digest_string_map digest_map;

            auto file = File(path.c_str());
            digest_map[file.d_digest] = path;

            try {
                UploadAndStore(&digest_map, nullptr,
                               request.bypass_local_cache());
            }
            catch (const OutOfSpaceException &e) {
                error_message =
                    "Out of space error in `writeBlob()` for path \"" + path +
                    "\": " + e.what();
                logAndStoreMessage(error_message, status,
                                   grpc::StatusCode::RESOURCE_EXHAUSTED);
                resp->mutable_status()->CopyFrom(status);
                continue;
            }
            catch (const std::runtime_error &e) {
                error_message = "Runtime error uploading and storing path \"" +
                                path + "\": " + e.what();
                logAndStoreMessage(error_message, status,
                                   grpc::StatusCode::INTERNAL);
                resp->mutable_status()->CopyFrom(status);
                continue;
            }
            catch (const std::invalid_argument &e) {
                error_message =
                    "Invalid argument error uploading and storing path \"" +
                    path + "\": " + e.what();
                logAndStoreMessage(error_message, status,
                                   grpc::StatusCode::INTERNAL);
                resp->mutable_status()->CopyFrom(status);
                continue;
            }

            resp->mutable_digest()->CopyFrom(file.d_digest);
            resp->set_path(path);
            status.set_code(grpc::StatusCode::OK);
        }
        catch (const std::runtime_error &e) {
            error_message =
                std::string(e.what()) + " thrown for path: " + path;
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::INTERNAL);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }
        resp->mutable_status()->CopyFrom(status);
    }
    return grpc::Status::OK;
}

const Digest LocalCasInstance::UploadAndStore(
    buildboxcommon::digest_string_map *const digest_map, const Tree *t, bool)
{
    Directory d;
    for (const auto &it : *digest_map) {
        // Check if string is a directory or file.Directory d;
        if (d.ParseFromString(it.second)) {
            d_storage->writeBlob(it.first, it.second);
        }
        else {
            std::string file_content =
                FileUtils::get_file_contents(it.second.c_str());
            d_storage->writeBlob(it.first, file_content);
        }
    }

    Digest tree_digest = Digest();
    if (t != nullptr) {
        // Get and emplace back tree_digest.
        tree_digest = buildboxcommon::make_digest(*t);
        const std::string tree_message_string = t->SerializeAsString();

        d_storage->writeBlob(tree_digest, tree_message_string);
    }

    return tree_digest;
}

Status LocalCasInstance::FetchTree(const FetchTreeRequest &request,
                                   FetchTreeResponse *)
{
    if (treeIsAvailableLocally(request.root_digest(),
                               request.fetch_file_blobs())) {
        return grpc::Status::OK;
    }
    else {
        return grpc::Status(
            grpc::StatusCode::NOT_FOUND,
            "The tree is not available in the local CAS or incomplete.");
    }
}
