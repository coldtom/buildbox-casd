/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_CASINSTANCEMANAGER_H
#define INCLUDED_BUILDBOXCASD_CASINSTANCEMANAGER_H

#include <map>
#include <memory>
#include <mutex>
#include <string>

#include <buildboxcasd_casinstance.h>

namespace buildboxcasd {

class CasInstanceManager {
  public:
    CasInstanceManager();

    bool addInstance(const std::string &instance_name,
                     std::shared_ptr<CasInstance> instance);

    std::shared_ptr<CasInstance> getInstance(const std::string &instance_name);

  private:
    std::map<std::string, std::shared_ptr<CasInstance>> d_instance_map;
    std::mutex d_instance_map_mutex;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_CASINSTANCEMANAGER_H
