/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_lrulocalcas.h>

#include <queue>
#include <stdexcept>

#include <buildboxcommon_logging.h>

using namespace buildboxcasd;
using namespace std::chrono_literals;

LruLocalCas::LruLocalCas(std::unique_ptr<LocalCas> base_cas, int64_t quota_low,
                         int64_t quota_high, int64_t reserved_space)
    : d_base_cas(std::move(base_cas)), d_configured_quota_low(quota_low),
      d_configured_quota_high(quota_high), d_reserved_space(reserved_space),
      d_protect_active_blobs(false)
{
    BUILDBOX_LOG_INFO("Initializing LRU with quota: " << quota_low << "/"
                                                      << quota_high);
    BUILDBOX_LOG_INFO("Reserved disk space: " << reserved_space);

    d_disk_usage = d_base_cas->getDiskUsage();

    // Set initial effective disk quota based on available disk space.
    updateDiskQuota();

    // Start thread to monitor available disk space.
    d_disk_monitor_thread = std::thread(&LruLocalCas::diskMonitorThread, this);
}

LruLocalCas::~LruLocalCas()
{
    // Stop and wait for disk monitor thread.
    {
        std::lock_guard<std::mutex> lock(d_stop_mutex);
        d_stop = true;
    }
    d_stop_cv.notify_all();
    d_disk_monitor_thread.join();
}

void LruLocalCas::protectActiveBlobs(blob_time_type start_time)
{
    d_protect_active_blobs = true;
    d_protect_active_time = start_time;
}

bool LruLocalCas::hasBlob(const Digest &digest)
{
    return d_base_cas->hasBlob(digest);
}

bool LruLocalCas::writeBlob(const Digest &digest, const std::string &data)
{
    const auto write_function = [&] {
        return d_base_cas->writeBlob(digest, data);
    };

    return tryToAddBlobAndUpdateDiskUsage(digest.size_bytes(), write_function);
}

bool LruLocalCas::moveBlob(const Digest &digest, const std::string &path)
{
    const auto write_function = [&] {
        return d_base_cas->moveBlob(digest, path);
    };

    return tryToAddBlobAndUpdateDiskUsage(digest.size_bytes(), write_function);
}

std::string LruLocalCas::readBlob(const Digest &digest, size_t offset,
                                  size_t length)
{
    return d_base_cas->readBlob(digest, offset, length);
}

std::string LruLocalCas::readBlob(const Digest &digest)
{
    return d_base_cas->readBlob(digest);
}

bool LruLocalCas::deleteBlob(const Digest &digest)
{
    if (d_base_cas->deleteBlob(digest)) {
        d_disk_usage -= static_cast<unsigned long>(digest.size_bytes());
        return true;
    }
    else {
        return false;
    }
}

void LruLocalCas::listBlobs(digest_time_callback_type callback)
{
    d_base_cas->listBlobs(callback);
}

std::string LruLocalCas::path(const Digest &digest)
{
    return d_base_cas->path(digest);
}

buildboxcommon::TemporaryDirectory LruLocalCas::createTemporaryDirectory()
{
    return d_base_cas->createTemporaryDirectory();
}

buildboxcommon::TemporaryFile LruLocalCas::createTemporaryFile()
{
    return d_base_cas->createTemporaryFile();
}

int64_t LruLocalCas::getDiskUsage() { return d_disk_usage; }

int64_t LruLocalCas::getDiskQuota() { return d_effective_quota_high; }

void LruLocalCas::verifyDiskUsage()
{
    auto actual_disk_usage = d_base_cas->getDiskUsage();

    if (d_disk_usage != actual_disk_usage) {
        std::stringstream error_message;
        error_message << "Cached disk usage " << d_disk_usage
                      << " does not match calculated disk usage "
                      << actual_disk_usage;
        throw std::runtime_error(error_message.str());
    }
}

int64_t LruLocalCas::getAvailableDiskSpace()
{
    return d_base_cas->getAvailableDiskSpace();
}

void LruLocalCas::updateDiskQuota()
{
    int64_t available_disk_space = getAvailableDiskSpace();

    int64_t max_quota = std::max<int64_t>(
        d_disk_usage + available_disk_space - d_reserved_space, 0);

    if (d_configured_quota_high > max_quota) {
        // Insufficient disk space for configured quota.
        // Use maximum possible quota for the high watermark and use the
        // same ratio between low watermark and high watermark as configured.
        double quota_low_high_ratio =
            static_cast<double>(d_configured_quota_low) /
            static_cast<double>(d_configured_quota_high);
        d_effective_quota_high = max_quota;
        d_effective_quota_low =
            static_cast<int64_t>(static_cast<double>(d_effective_quota_high) *
                                 quota_low_high_ratio);
    }
    else {
        // There is sufficient disk space available, use configured quota.
        d_effective_quota_high = d_configured_quota_high;
        d_effective_quota_low = d_configured_quota_low;
    }
}

void LruLocalCas::diskMonitorThread()
{
    // Periodically check available disk space
    const auto interval = 5s;

    std::unique_lock<std::mutex> lock(d_stop_mutex);
    while (!d_stop_cv.wait_for(lock, interval, [&] { return d_stop; })) {
        updateDiskQuota();

        if (d_disk_usage >= d_effective_quota_high) {
            // High watermark reached. Trigger expiry and wait for it to
            // complete.
            cleanup();
        }
    }
}

void LruLocalCas::cleanup()
{
    std::lock_guard<std::mutex> lock(d_cleanup_mutex);

    int64_t quota_high = d_effective_quota_high;
    int64_t quota_low = d_effective_quota_low;

    if (d_disk_usage < quota_high) {
        // Another thread beat us to it, don't trigger another cleanup
        return;
    }

    BUILDBOX_LOG_INFO("Starting LRU cleanup. Disk usage: " << d_disk_usage);

    using time_digest_pair = std::pair<blob_time_type, Digest>;

    auto pq_compare = [](time_digest_pair &a, time_digest_pair &b) {
        return a.first > b.first;
    };

    std::priority_queue<time_digest_pair, std::vector<time_digest_pair>,
                        decltype(pq_compare)>
        pq(pq_compare);

    listBlobs([&pq](const buildboxcommon::Digest &digest,
                    buildboxcasd::blob_time_type time) {
        pq.push(std::make_pair(time, digest));
    });

    while (d_disk_usage > quota_low && !pq.empty()) {
        if (d_protect_active_blobs) {
            auto blob_time = pq.top().first;
            if (blob_time >= d_protect_active_time) {
                // Blob was created or used after the start point of active
                // blob protection. Do not delete blob.

                if (d_disk_usage >= quota_high) {
                    // Disk usage still above high watermark. This means that
                    // the working set is larger than the quota and we can't
                    // continue.

                    const std::string error_message =
                        "Insufficient storage quota";
                    BUILDBOX_LOG_ERROR(error_message);
                    throw OutOfSpaceException(error_message);
                }

                break;
            }
        }

        auto digest = pq.top().second;
        pq.pop();

        try {
            deleteBlob(digest);
        }
        catch (const std::runtime_error &e) {
            BUILDBOX_LOG_ERROR("Error deleting blob " << digest.hash() << "/"
                                                      << digest.size_bytes()
                                                      << ": " << e.what());
        }
    }

    BUILDBOX_LOG_INFO("Finished LRU cleanup. Disk usage: " << d_disk_usage);
}

LocalCas *LruLocalCas::getBase() { return d_base_cas.get(); }

bool LruLocalCas::tryToAddBlobAndUpdateDiskUsage(
    google::protobuf::int64 blob_size,
    const std::function<bool()> &add_blob_function)
{
    if (blob_size >= d_effective_quota_high) {
        // Blob is larger than quota. Do not attempt cleanup and bail out.
        std::ostringstream error_message;
        error_message << "Blob size exceeds total storage allocation ("
                      << blob_size << " >= " << d_effective_quota_high
                      << " bytes)";
        BUILDBOX_LOG_ERROR(error_message.str());
        throw OutOfSpaceException(error_message.str());
    }

    // Reserve disk usage before actual write to ensure expiry is triggered
    // early enough in case of parallel write requests.
    d_disk_usage += blob_size;

    try {
        if (d_disk_usage >= d_effective_quota_high) {
            // High watermark reached. Trigger expiry and wait for it to
            // complete.
            cleanup();
        }

        const bool blob_was_written = add_blob_function();
        if (blob_was_written) {
            return true;
        }
        else {
            // No error but disk usage has not increased.
            d_disk_usage -= blob_size;
            return false;
        }
    }
    catch (...) {
        d_disk_usage -= blob_size;
        throw;
    }
}
