/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_CASSERVER_H
#define INCLUDED_BUILDBOXCASD_CASSERVER_H

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_context.h>

#include <buildboxcommon_protos.h>

#include <memory>
#include <regex>

#include <buildboxcasd_casinstance.h>
#include <buildboxcasd_casinstancemanager.h>
#include <buildboxcasd_localcas.h>

namespace buildboxcasd {

using namespace build::bazel::remote::execution::v2;
using namespace build::buildgrid;
using namespace google::protobuf::util;
using namespace google::protobuf;
using namespace google::bytestream;

using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::Status;

class CapabilitiesServicer final : public Capabilities::Service {
  public:
    explicit CapabilitiesServicer(
        std::shared_ptr<CasInstanceManager> instance_manager);

    Status GetCapabilities(grpc::ServerContext *,
                           const GetCapabilitiesRequest *request,
                           ServerCapabilities *response) override;

  private:
    std::shared_ptr<CasInstanceManager> d_instance_manager;
    ServerCapabilities d_server_capabilities;
};

class CasRemoteExecutionServicer final
    : public ContentAddressableStorage::Service {
    /* Implements the CAS RPC methods. */

  public:
    explicit CasRemoteExecutionServicer(
        std::shared_ptr<CasInstanceManager> instance_manager);

    Status FindMissingBlobs(ServerContext *,
                            const FindMissingBlobsRequest *request,
                            FindMissingBlobsResponse *response) override;

    Status BatchUpdateBlobs(ServerContext *,
                            const BatchUpdateBlobsRequest *request,
                            BatchUpdateBlobsResponse *response) override;

    Status BatchReadBlobs(ServerContext *,
                          const BatchReadBlobsRequest *request,
                          BatchReadBlobsResponse *response) override;

    Status GetTree(ServerContext *, const GetTreeRequest *request,
                   ServerWriter<GetTreeResponse> *writer) override;

  private:
    std::shared_ptr<CasInstanceManager> d_instance_manager;
};

class CasBytestreamServicer final : public ByteStream::Service {
    /* Implements the Bytestream API Read() and Write() methods for the CAS. */
  public:
    explicit CasBytestreamServicer(
        std::shared_ptr<CasInstanceManager> instance_manager);

    Status Read(grpc::ServerContext *, const ReadRequest *request,
                ServerWriter<ReadResponse> *writer) override;

    Status Write(grpc::ServerContext *, ServerReader<WriteRequest> *request,
                 WriteResponse *response) override;

  private:
    std::string readInstanceName(const std::string &resource_name) const;
    std::string writeInstanceName(const std::string &resource_name) const;

    std::string
    instanceNameFromResourceName(const std::string &resource_name,
                                 const std::string &root_path) const;

    std::shared_ptr<CasInstanceManager> d_instance_manager;
};

class LocalCasServicer final : public LocalContentAddressableStorage::Service {
    /* Implements the LocalContentAddressableStorage RPC methods */

  public:
    LocalCasServicer(std::shared_ptr<CasInstanceManager> instance_manager,
                     std::shared_ptr<LocalCas> storage);

    Status FetchMissingBlobs(ServerContext *context,
                             const FetchMissingBlobsRequest *request,
                             FetchMissingBlobsResponse *response) override;

    Status UploadMissingBlobs(ServerContext *context,
                              const UploadMissingBlobsRequest *request,
                              UploadMissingBlobsResponse *response) override;

    Status FetchTree(ServerContext *context, const FetchTreeRequest *request,
                     FetchTreeResponse *response) override;

    Status UploadTree(ServerContext *context, const UploadTreeRequest *request,
                      UploadTreeResponse *response) override;

    Status StageTree(ServerContext *context,
                     ServerReaderWriter<StageTreeResponse, StageTreeRequest>
                         *stream) override;

    Status CaptureTree(ServerContext *context,
                       const CaptureTreeRequest *request,
                       CaptureTreeResponse *response) override;

    Status CaptureFiles(ServerContext *context,
                        const CaptureFilesRequest *request,
                        CaptureFilesResponse *response) override;

    Status GetInstanceNameForRemote(
        ServerContext *context, const GetInstanceNameForRemoteRequest *request,
        GetInstanceNameForRemoteResponse *response) override;

    Status GetLocalDiskUsage(ServerContext *context,
                             const GetLocalDiskUsageRequest *request,
                             GetLocalDiskUsageResponse *response) override;

  private:
    std::shared_ptr<CasInstanceManager> d_instance_manager;

    std::shared_ptr<LocalCas> d_storage;
};

class CasService {
    /* Class that provides the `ContentAddressableStorage` methods specified
     * by the remote execution API.
     * The implementation is split into two servicers, one employing
     * the Bytestream API.
     */
  public:
    CasService(std::shared_ptr<LocalCas> cas_storage);

    CasService(std::shared_ptr<LocalCas> cas_storage,
               const std::string &instance_name);

    CasService(std::shared_ptr<LocalCas> cas_storage,
               std::shared_ptr<buildboxcommon::Client> cas_client,
               const std::string &instance_name);

    inline std::shared_ptr<grpc::Service> remoteExecutionCasServicer()
    {
        return d_remote_execution_cas_servicer;
    }
    inline std::shared_ptr<grpc::Service> bytestreamServicer()
    {
        return d_cas_bytestream_servicer;
    }
    inline std::shared_ptr<grpc::Service> localCasServicer()
    {
        return d_local_cas_servicer;
    }
    inline std::shared_ptr<grpc::Service> capabilitiesServicer()
    {
        return d_capabilities_servicer;
    }

  private:
    std::shared_ptr<LocalCas> d_cas_storage;
    std::shared_ptr<CasInstanceManager> d_instance_manager;

    const std::shared_ptr<CasRemoteExecutionServicer>
        d_remote_execution_cas_servicer;
    const std::shared_ptr<CasBytestreamServicer> d_cas_bytestream_servicer;
    const std::shared_ptr<LocalCasServicer> d_local_cas_servicer;
    const std::shared_ptr<CapabilitiesServicer> d_capabilities_servicer;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_CASSERVER_H
