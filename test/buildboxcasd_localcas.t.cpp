﻿/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localcas.h>

#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_temporarydirectory.h>
#include <buildboxcommon_temporaryfile.h>

#include <gtest/gtest.h>

using namespace buildboxcasd;

class LocalCasFixture : public ::testing::Test {
  protected:
    LocalCasFixture() : cas(cas_root_directory.name()) {}

    buildboxcommon::TemporaryDirectory cas_root_directory;
    FsLocalCas cas;

    static void writeToFile(const std::string &data, const std::string &path)
    {
        ASSERT_TRUE(buildboxcommon::FileUtils::is_regular_file(path.c_str()));
        std::ofstream file(path, std::ofstream::binary);
        file << data;
        file.close();
    }

    static Digest make_digest(const std::string &data)
    {
        return buildboxcommon::CASHash::hash(data);
    }

    void assertFilePermissionsInCasAreValid(const Digest &digest)
    {
        // Other processess are allowed to access the cache directory directly,
        // so we expect files to be readable and writable by casd and only
        // readable for everybody else (0644).

        const std::string path_in_cas = cas.path(digest);
        const mode_t expected_permissions =
            S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

        struct stat statbuf;
        ASSERT_EQ(stat(path_in_cas.c_str(), &statbuf), 0);

        const mode_t file_permissions =
            (statbuf.st_mode & static_cast<mode_t>(~S_IFMT));
        ASSERT_EQ(file_permissions, expected_permissions);
    }
};

TEST_F(LocalCasFixture, EmptyCasDirectoryStructure)
{
    const auto cas_objects_path =
        std::string(cas_root_directory.name()) + "/cas/objects/";
    const auto temp_objects_path =
        std::string(cas_root_directory.name()) + "/cas/tmp/";

    ASSERT_EQ(access(cas_objects_path.c_str(), F_OK), 0);
    ASSERT_EQ(access(temp_objects_path.c_str(), F_OK), 0);
}

TEST_F(LocalCasFixture, TestEmpty)
{
    ASSERT_FALSE(cas.hasBlob(make_digest("data")));
}

TEST_F(LocalCasFixture, WriteAndReadEmptyBlob)
{
    Digest digest = make_digest("");
    cas.writeBlob(digest, "");

    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest), "");
}

TEST_F(LocalCasFixture, WriteWithInvalidHash)
{
    const std::string data = "data";
    Digest digest = make_digest(data);

    digest.set_hash(digest.hash() + "a");

    ASSERT_THROW(cas.writeBlob(digest, data), std::invalid_argument);
}

TEST_F(LocalCasFixture, WriteWithInvalidSize)
{
    const std::string data = "data";
    Digest digest = make_digest(data);

    digest.set_size_bytes(digest.size_bytes() + 1);

    const auto invalid_data_size =
        static_cast<google::protobuf::int64>(data.size() + 1);
    digest.set_size_bytes(invalid_data_size);

    ASSERT_THROW(cas.writeBlob(digest, data), std::invalid_argument);
}

TEST_F(LocalCasFixture, TestWriteAndRead)
{
    const std::string data = "someData";
    const auto digest = make_digest(data);

    ASSERT_FALSE(cas.hasBlob(digest));

    cas.writeBlob(digest, data);

    // Data was written and is valid:
    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest), data);
    ASSERT_EQ(cas.readBlob(digest, 0, static_cast<size_t>(data.size())), data);
}

TEST_F(LocalCasFixture, TestWriteAndReadBinaryData)
{
    std::vector<char> raw_data = {'H', 'e', 'l', 'l', 'o',  '\0', 'W',
                                  'o', 'r', 'l', 'd', '\0', '!'};

    const std::string data_string(raw_data.cbegin(), raw_data.cend());
    ASSERT_EQ(data_string.size(), raw_data.size());

    const auto digest = make_digest(data_string);
    ASSERT_FALSE(cas.hasBlob(digest));
    cas.writeBlob(digest, data_string);
    ASSERT_TRUE(cas.hasBlob(digest));

    std::string read_data = cas.readBlob(digest);
    ASSERT_TRUE(
        std::equal(read_data.cbegin(), read_data.cend(), raw_data.cbegin()));
    ASSERT_EQ(read_data, data_string);
}

TEST_F(LocalCasFixture, WriteBlobFilePermissions)
{
    const std::string data = "Some data...";
    const Digest digest = make_digest(data);

    cas.writeBlob(digest, data);
    ASSERT_TRUE(cas.hasBlob(digest));

    assertFilePermissionsInCasAreValid(digest);
}

TEST_F(LocalCasFixture, ReadMissingBlobThrows)
{
    const auto digest = make_digest("dataNotPresent");
    ASSERT_FALSE(cas.hasBlob(digest));
    ASSERT_THROW(cas.readBlob(digest), BlobNotFoundException);
}

TEST_F(LocalCasFixture, ReadEmptyBlob)
{
    const auto digest = make_digest("");
    cas.writeBlob(digest, "");

    ASSERT_EQ(cas.readBlob(digest, 0, LocalCas::npos), "");
}

TEST_F(LocalCasFixture, ReadEmptyBlobWithZeroLength)
{
    const auto digest = make_digest("");
    cas.writeBlob(digest, "");

    ASSERT_EQ(cas.readBlob(digest, 0, 0), "");
}

TEST_F(LocalCasFixture, ReadEmptyBlobWithPositiveOffsetAndZeroLength)
{
    const auto digest = make_digest("");
    cas.writeBlob(digest, "");

    ASSERT_THROW(cas.readBlob(digest, 1, 0), std::out_of_range);
}

TEST_F(LocalCasFixture, ReadEmptyBlobWithPositiveOffsetAndNposLength)
{
    const auto digest = make_digest("");
    cas.writeBlob(digest, "");

    ASSERT_THROW(cas.readBlob(digest, 1, LocalCas::npos), std::out_of_range);
}

TEST_F(LocalCasFixture, EmptyRead)
{
    const auto data = "alpha";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_EQ(cas.readBlob(digest, 1, 0), "");
}

TEST_F(LocalCasFixture, PartialRead)
{
    const auto data = "alpha bravo charlie";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_EQ(cas.readBlob(digest, 6, 5), "bravo");
}

TEST_F(LocalCasFixture, InvalidOffsetThrows)
{
    const auto data = "123";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_THROW(cas.readBlob(digest, 4, 1), std::out_of_range);
}

TEST_F(LocalCasFixture, ReadingUntilEndFromDataSize)
{
    const std::string data = "abcde";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_EQ(cas.readBlob(digest, data.size(), LocalCas::npos), "");
}

TEST_F(LocalCasFixture, ExcessiveLengthThrows)
{
    const std::string data = "abcde";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_THROW(cas.readBlob(digest, 1, static_cast<size_t>(data.size())),
                 std::out_of_range);
}

TEST_F(LocalCasFixture, NposBytesReadsUntilEnd)
{
    const auto data = "-abcdefghi";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_EQ(cas.readBlob(digest, 1, buildboxcasd::LocalCas::npos),
              "abcdefghi");
}

TEST_F(LocalCasFixture, TestWriteOutputs)
{
    const auto data = "data123";
    const auto digest = make_digest(data);
    ASSERT_FALSE(cas.hasBlob(digest));

    cas.writeBlob(digest, data);

    // Checking that the physical file is where we expect it:
    const auto cas_objects_path =
        std::string(cas_root_directory.name()) + "/cas/objects";
    const auto parent_subdirectory_name = digest.hash().substr(0, 2);
    const auto filename = digest.hash().substr(2);

    const auto expected_file_path =
        cas_objects_path + "/" + parent_subdirectory_name + "/" + filename;

    // File exists:
    ASSERT_EQ(access(expected_file_path.c_str(), F_OK), 0);

    // And the data stored in it is what we wrote:
    std::ifstream file(expected_file_path);
    ASSERT_TRUE(file.good());

    std::stringstream file_contents;
    file_contents << file.rdbuf();
    ASSERT_TRUE(file_contents.good());

    ASSERT_EQ(file_contents.str(), data);
}

TEST_F(LocalCasFixture, GetPath)
{
    const auto data = "data";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    const size_t hash_prefix_length =
        static_cast<size_t>(cas.pathHashPrefixLength());

    const std::string expected_path =
        std::string(cas_root_directory.name()) + "/cas/objects/" +
        digest.hash().substr(0, hash_prefix_length) + "/" +
        digest.hash().substr(hash_prefix_length, std::string::npos);

    ASSERT_EQ(cas.path(digest), expected_path);
}

TEST_F(LocalCasFixture, GetPathForBlobNotPresentThrows)
{
    const auto digest = make_digest("Data not present in the LocalCAS.");
    ASSERT_FALSE(cas.hasBlob(digest));
    ASSERT_THROW(cas.path(digest), BlobNotFoundException);
}

TEST_F(LocalCasFixture, TempDirectoryInsideTmpDir)
{
    const auto temp_dir = cas.createTemporaryDirectory();

    const auto temp_objects_path =
        std::string(cas_root_directory.name()) + "/cas/tmp/cas-tmp";

    ASSERT_EQ(
        std::string(temp_dir.name()).substr(0, temp_objects_path.length()),
        temp_objects_path);
}

TEST_F(LocalCasFixture, TempFileInsideTmpDir)
{
    const auto temp_file = cas.createTemporaryFile();

    const auto temp_objects_path =
        std::string(cas_root_directory.name()) + "/cas/tmp/cas-tmp";

    ASSERT_EQ(
        std::string(temp_file.name()).substr(0, temp_objects_path.length()),
        temp_objects_path);
}

TEST_F(LocalCasFixture, MoveBlob)
{
    // Creating a temporary file inside the CAS temp directory and writing some
    // data to it:
    const auto temp_file = cas.createTemporaryFile();
    const std::string data = "Hello. This is some data.";
    const Digest digest = make_digest(data);
    writeToFile(data, temp_file.name());

    ASSERT_FALSE(cas.hasBlob(digest));
    // We'll now move the file into the LocalCAS. (Since the file isn't
    // there, the move is performed and `moveBlob()` returns `true`.)
    ASSERT_TRUE(cas.moveBlob(digest, temp_file.name()));

    // The data is correct:
    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest), data);

    // Being a move, we expect the the source file to be deleted:
    ASSERT_FALSE(buildboxcommon::FileUtils::is_regular_file(temp_file.name()));
}

TEST_F(LocalCasFixture, MoveBlobFinalFilePermissions)
{
    // Creating a temporary file inside the CAS temp directory and writing some
    // data to it:
    const auto temp_file = cas.createTemporaryFile();
    const std::string data = "Hello. This is some data.";
    const Digest digest = make_digest(data);
    writeToFile(data, temp_file.name());

    ASSERT_FALSE(cas.hasBlob(digest));

    // Moving it into the LocalCAS:
    ASSERT_TRUE(cas.moveBlob(digest, temp_file.name()));
    ASSERT_TRUE(cas.hasBlob(digest));

    assertFilePermissionsInCasAreValid(digest);
}

TEST_F(LocalCasFixture, MoveExistingBlob)
{
    // Creating a temporary file inside the CAS temp directory and writing some
    // data to it:
    const auto temp_file = cas.createTemporaryFile();
    const std::string data = "Hello. This is some data.";
    const Digest digest = make_digest(data);
    writeToFile(data, temp_file.name());

    // We want the blob to be present before the move:
    cas.writeBlob(digest, data);
    ASSERT_TRUE(cas.hasBlob(digest));

    // Now we'll move the file into the LocalCAS. (Since the blob is
    // already in the CAS, `moveBlob()` returns `false`.)
    ASSERT_FALSE(cas.moveBlob(digest, temp_file.name()));

    // The move is still performed, and the source file deleted:
    ASSERT_FALSE(buildboxcommon::FileUtils::is_regular_file(temp_file.name()));

    // The data is correct:
    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest), data);
}

TEST_F(LocalCasFixture, MoveBlobWithInvalidDigest)
{
    // Creating a temporary file inside the CAS temp directory and writing some
    // data to it:
    const auto temp_file = cas.createTemporaryFile();
    const std::string data = "Hello. This is some data.";
    writeToFile(data, temp_file.name());

    Digest digest;
    digest.set_hash("NOT a valid hash.");
    digest.set_size_bytes(static_cast<google::protobuf::int64>(data.size()));

    // We'll now attempt to move the file into the LocalCAS:
    ASSERT_THROW(cas.moveBlob(digest, temp_file.name()),
                 std::invalid_argument);

    // Since the move failed, the source file is still there:
    ASSERT_TRUE(buildboxcommon::FileUtils::is_regular_file(temp_file.name()));
}

TEST_F(LocalCasFixture, MoveBlobWithPathOutsideCASTempDir)
{
    // Creating an arbitrary temporary file in a directory that is not the CAS
    // temp directory and writing some data to it:
    buildboxcommon::TemporaryFile temp_file;
    const std::string data = "Hello. This is some data.";
    const Digest digest = make_digest(data);
    writeToFile(data, temp_file.name());

    // This is not allowed by `moveBlob()`, which expects a file created with
    // `cas.createTemporaryFile()`:
    ASSERT_THROW(cas.moveBlob(digest, temp_file.name()),
                 std::invalid_argument);

    // Since the move failed, the source file is still there:
    ASSERT_TRUE(buildboxcommon::FileUtils::is_regular_file(temp_file.name()));
}
