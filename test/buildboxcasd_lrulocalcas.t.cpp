/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localcas.h>
#include <buildboxcasd_lrulocalcas.h>

#include <unistd.h>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_temporarydirectory.h>

#include <fstream>
#include <gtest/gtest.h>

using namespace buildboxcasd;

class LruLocalCasFixture : public ::testing::Test {
  protected:
    LruLocalCasFixture()
        : cas(std::make_unique<FsLocalCas>(cas_root_directory.name()), 2048,
              4096, 0)
    {
    }

    buildboxcommon::TemporaryDirectory cas_root_directory;
    LruLocalCas cas;

    void writeToFile(const std::string &data, const std::string path)
    {
        std::ofstream file(path, std::ofstream::binary);
        file << data;
        file.close();
        ASSERT_TRUE(buildboxcommon::FileUtils::is_regular_file(path.c_str()));
    }
};

inline static Digest make_digest(const std::string &data)
{
    return buildboxcommon::CASHash::hash(data);
}

// Test identical to FsLocalCas test
TEST_F(LruLocalCasFixture, TestWriteAndRead)
{
    const std::string data = "someData";
    const auto digest = make_digest(data);

    ASSERT_FALSE(cas.hasBlob(digest));

    cas.writeBlob(digest, data);

    // Data was written and is valid:
    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest), data);
    ASSERT_EQ(cas.readBlob(digest, 0, static_cast<size_t>(data.size())), data);
}

TEST_F(LruLocalCasFixture, GetPath)
{
    const auto data = "data";
    const auto digest = make_digest(data);
    cas.writeBlob(digest, data);

    ASSERT_EQ(cas.path(digest), cas.getBase()->path(digest));
}

TEST_F(LruLocalCasFixture, TestDelete)
{
    const std::string data = "someData";
    const auto digest = make_digest(data);

    ASSERT_FALSE(cas.hasBlob(digest));

    // Write blob
    cas.writeBlob(digest, data);
    ASSERT_TRUE(cas.hasBlob(digest));

    // Delete blob and verify it has been deleted
    ASSERT_TRUE(cas.deleteBlob(digest));
    ASSERT_FALSE(cas.hasBlob(digest));

    // Try deleting it again
    ASSERT_FALSE(cas.deleteBlob(digest));
}

TEST_F(LruLocalCasFixture, Expire)
{
    auto digests = std::vector<Digest>(6);

    for (int i = 0; i < digests.size(); i++) {
        const auto data = std::string(1000, '0' + i);
        digests[i] = make_digest(data);
        cas.writeBlob(digests[i], data);

        // Wait 10 ms between writes to get different timestamps.
        // This should be a sufficiently long delay even with the lowest Linux
        // kernel timer frequency (100 Hz).
        usleep(10000);
    }

    for (int i = 0; i < digests.size(); i++) {
        // Writing the fifth 1000 Byte blob should
        // expire the first three 1000 Byte blobs.
        if (i < 3) {
            ASSERT_FALSE(cas.hasBlob(digests[i]));
        }
        else {
            ASSERT_TRUE(cas.hasBlob(digests[i]));
        }
    }
}

TEST_F(LruLocalCasFixture, DiskUsage)
{
    cas.verifyDiskUsage();

    for (int i = 0; i < 10; i++) {
        const auto data = std::string(1000, '0' + i);
        const auto digest = make_digest(data);
        cas.writeBlob(digest, data);
        cas.verifyDiskUsage();
    }
}

TEST_F(LruLocalCasFixture, DiskUsageDuplicates)
{
    cas.verifyDiskUsage();

    for (int i = 0; i < 20; i++) {
        const auto data = std::string(1000, '0' + i / 2);
        const auto digest = make_digest(data);
        cas.writeBlob(digest, data);
        cas.verifyDiskUsage();
    }
}

TEST_F(LruLocalCasFixture, MoveBlobUpdatesDiskUsage)
{
    // Creating a temporary file inside the CAS temp directory and writing some
    // data to it:
    const auto temp_file = cas.createTemporaryFile();
    const std::string data = "Hello. This is some data.";
    const Digest digest = make_digest(data);
    writeToFile(data, temp_file.name());

    ASSERT_FALSE(cas.hasBlob(digest));
    // We'll now move that file to the CAS.
    // (Since the file isn't there, the move is performed and `moveBlob()`
    // returns `true`.)
    const auto disk_usage_before_move = cas.getDiskUsage();
    ASSERT_TRUE(cas.moveBlob(digest, temp_file.name()));

    // The data is correct:
    ASSERT_TRUE(cas.hasBlob(digest));
    ASSERT_EQ(cas.readBlob(digest), data);

    // And the disk usage increased accordingly:
    ASSERT_EQ(cas.getDiskUsage(),
              disk_usage_before_move + static_cast<int64_t>(data.size()));
}

TEST_F(LruLocalCasFixture, MoveExistingBlobUpdatesDiskUsage)
{
    // Creating a temporary file inside the CAS temp directory and writing some
    // data to it:
    const auto temp_file = cas.createTemporaryFile();
    const std::string data = "Hello. This is some data.";
    const Digest digest = make_digest(data);
    writeToFile(data, temp_file.name());

    // We want the blob to be present in the CAS before the move:
    cas.writeBlob(digest, data);
    ASSERT_TRUE(cas.hasBlob(digest));

    const auto disk_usage_before_move = cas.getDiskUsage();

    // We'll now move that file to the CAS.
    // (Since the file is already stored, `moveBlob()` returns `false`.)
    ASSERT_FALSE(cas.moveBlob(digest, temp_file.name()));

    // The disk usage did not change:
    ASSERT_EQ(cas.getDiskUsage(), disk_usage_before_move);
}
